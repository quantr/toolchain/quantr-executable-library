package hk.quantr.executablelibrary;

import hk.quantr.executablelibrary.elf32.ELF32;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestRiscVElfRead {

	@Test
	public void testRead() {
		try {
			InputStream inputStream = TestRiscVElfRead.class.getResourceAsStream("a.elf");
			ELF32 elf = new ELF32(inputStream);

			System.out.println(elf);
			
		
		} catch (Exception ex) {
			Logger.getLogger(TestRiscVElfRead.class.getName()).log(Level.SEVERE, null, ex);
		}

	}
}
