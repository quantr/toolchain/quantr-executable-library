package hk.quantr.executablelibrary;

import hk.quantr.executablelibrary.dwarf.DwarfData;
import hk.quantr.executablelibrary.elf32.ELF32;
import hk.quantr.executablelibrary.elf32.ELF32WriteSection;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestRiscVElfWrite {

	@Test
	public void testWrite() {
		try {
			//command: readelf --debug-dump=decodedline out.bin
			ArrayList<DwarfData> files = new ArrayList();
			files.add(new DwarfData("a.c", 0, 0x100));
			files.add(new DwarfData("a.c", 1, 0x101));

			File file = new File("a.elf");
//			FileOutputStream fo = new FileOutputStream(file);
			RandomAccessFile fo = new RandomAccessFile(file, "rw");

			ELF32 elf = new ELF32();
			elf.writeSections.add(new ELF32WriteSection(".text", new byte[]{(byte) 0xb8, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xbf, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00}));
			elf.writeSections.add(new ELF32WriteSection(".data", new byte[]{1, 2, 3, 4}));
			elf.writeSections.add(new ELF32WriteSection(".shstrtab", new byte[]{4, 5, 6}));

			/*ELF64 elf = new ELF64();
			elf.writeSections.add(new ELF64WriteSection(".text", new byte[]{(byte) 0xb8, 0x01, 0x00, 0x00, 0x00, (byte) 0xbf, 0x01, 0x00, 0x00, 0x00, 0x48, (byte) 0xbe, (byte) 0xd8, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, (byte) 0xba, 0x0d, 0x00, 0x00, 0x00, 0x0f, 0x05, (byte) 0xb8, 0x3c, 0x00, 0x00, 0x00, 0x48, 0x31, (byte) 0xff, 0x0f, 0x05}));
			elf.writeSections.add(new ELF64WriteSection(".data", new byte[]{1, 2, 3}));
			elf.writeSections.add(new ELF64WriteSection(".shstrtab", new byte[]{4, 5, 6}));*/
			elf.write(fo);
			fo.close();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(TestElfRead.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(TestRiscVElfWrite.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
