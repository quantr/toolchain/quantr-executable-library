package hk.quantr.executablelibrary;

import hk.quantr.executablelibrary.elf64.ELF64;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestElfRead {

	@Test
	public void testRead() {
		try {
			InputStream inputStream = TestElfRead.class.getResourceAsStream("zip");
			ELF64 elf = new ELF64(inputStream);

			System.out.println(elf);
		} catch (Exception ex) {
			Logger.getLogger(TestElfRead.class.getName()).log(Level.SEVERE, null, ex);
		}

	}
}
