//package hk.quantr.executablelibrary;
//
//
//import hk.quantr.dwarf.elf.Elf32_Ehdr;
//import hk.quantr.javalib.CommonLib;
//import org.junit.Test;
//
///**
// *
// * @author Peter <peter@quantr.hk>
// */
//public class CreateRiscVElf {
//
//	@Test
//	public void test() {
//		Elf32_Ehdr ehdr = new Elf32_Ehdr();
//
//		ehdr.e_ident = CommonLib.hexStringToByteArray("7f454c46010101000000000000000000");
//		System.out.println(ehdr.e_ident);
////[ e_ident: 7f454c46010101000000000000000000; e_type: 2; e_machine: 243; e_version: 1; e_entry: 0x10054; e_phoff: 52; e_shoff: 436; e_flags: 0x0; e_ehsize: 52; e_phentsize: 32; e_phentnum: 1; e_shentsize: 40; e_shnum: 6; e_shstrndx: 5 ]
////[ number: 0; sh_name: 0; section_name: ; sh_type: 0; sh_flags: 0x0; sh_addr: 0x0; sh_offset: 0; sh_size: 0; sh_link: 0; sh_info: 0; sh_addralgin: 0; sh_entsize: 0 ]
////[ number: 1; sh_name: 27; section_name: .text; sh_type: 1; sh_flags: 0x6; sh_addr: 0x10054; sh_offset: 84; sh_size: 8; sh_link: 0; sh_info: 0; sh_addralgin: 4; sh_entsize: 0 ]
////[ number: 2; sh_name: 33; section_name: .riscv.attributes; sh_type: 1879048195; sh_flags: 0x0; sh_addr: 0x0; sh_offset: 92; sh_size: 26; sh_link: 0; sh_info: 0; sh_addralgin: 1; sh_entsize: 0 ]
////[ number: 3; sh_name: 1; section_name: .symtab; sh_type: 2; sh_flags: 0x0; sh_addr: 0x0; sh_offset: 120; sh_size: 176; sh_link: 4; sh_info: 3; sh_addralgin: 4; sh_entsize: 16 ]
////[ number: 4; sh_name: 9; section_name: .strtab; sh_type: 3; sh_flags: 0x0; sh_addr: 0x0; sh_offset: 296; sh_size: 86; sh_link: 0; sh_info: 0; sh_addralgin: 1; sh_entsize: 0 ]
////[ number: 5; sh_name: 17; section_name: .shstrtab; sh_type: 3; sh_flags: 0x0; sh_addr: 0x0; sh_offset: 382; sh_size: 51; sh_link: 0; sh_info: 0; sh_addralgin: 1; sh_entsize: 0 ]
//
//	}
//}
