/*
 * Copyright 2022 yin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary;

/**
 *
 * @author yin
 */

import hk.quantr.executablelibrary.qr.Header;
import hk.quantr.executablelibrary.qr.ProgramSegment;
import hk.quantr.executablelibrary.qr.QR;
import hk.quantr.executablelibrary.qr.Section;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

public class TestQRWrite {

	@Test
	public void testqrWrite() throws IOException {
		Header header = new Header("hello", "hi", "hi", "hi", "hi", "hi", "hi", 1, 2, 3, 4, 5, 6, 7, 8);
		ProgramSegment programSegment = new ProgramSegment("hi", "hi", 1, 2, 3, 4, 5, 6);
		ArrayList<ProgramSegment> programSegmentArrayList = new ArrayList<>();
		programSegmentArrayList.add(programSegment);
		Section section = new Section("hi", "hi", "hi", 1, 2, 3, "hi", 1, 2);
		ArrayList<Section> sectionArrayList = new ArrayList<>();
		sectionArrayList.add(section);
		QR qr = new QR(header, programSegmentArrayList, sectionArrayList);
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.writeValue(new File("qr.json"), qr);
	}
}
