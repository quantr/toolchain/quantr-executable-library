package hk.quantr.executablelibrary;

import hk.quantr.javalib.CommonLib;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestInputStream {

	@Test
	public void test2() {

		try {
			InputStream inputStream = TestInputStream.class.getResourceAsStream("dump.bin");
			
			inputStream.mark(0);

			System.out.println(inputStream.read());
			System.out.println(inputStream.read());
			System.out.println(inputStream.read());
			System.out.println(inputStream.read());

			inputStream.reset();
			// inputStream.skip(20);
			CommonLib.skip(inputStream, 50);

			System.out.println(inputStream.read());
			System.out.println(inputStream.read());
			System.out.println(inputStream.read());
			System.out.println(inputStream.read());

		} catch (Exception ex) {
			Logger.getLogger(TestInputStream.class.getName()).log(Level.SEVERE, null, ex);
		}

	}
}
