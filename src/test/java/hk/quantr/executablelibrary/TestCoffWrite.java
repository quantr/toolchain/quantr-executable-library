package hk.quantr.executablelibrary;

import hk.quantr.executablelibrary.coff.Coff;
import hk.quantr.executablelibrary.coff.CoffWriteSection;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestCoffWrite {

	@Test
	public void testRead() {
		try {
			File file = new File("out.obj");
			RandomAccessFile fo = new RandomAccessFile(file, "rw");

			Coff coff = new Coff();

//			coff.writeSections.add(new CoffWriteSection(".text", new byte[]{(byte) 0x89, (byte) 0xe5, (byte) 0x83, (byte) 0xec, 0x04, 0x6a, (byte) 0xf5, (byte) 0xe8, 0, 0, 0, 0, (byte) 0x89, (byte) 0xc3, 0x6a, 0,
//				(byte) 0x8d, 0x45, (byte) 0xfc, 0x50, 0x6a, 0x0d, 0x68, 0x29, 0, 0, 0, 0x53, (byte) 0xe8, 0, 0, 0,
//				0, 0x6a, 0, (byte) 0xe8, 0, 0, 0, 0, (byte) 0xf4, 0x48, 0x65, 0x6c, 0x6f, 0x2c, 0x20,
//				0x57, 0x6f, 0x72, 0x6c, 0x64, 0x0a}));

			coff.write(fo);
			fo.close();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(TestElfRead.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(TestElfWrite.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
