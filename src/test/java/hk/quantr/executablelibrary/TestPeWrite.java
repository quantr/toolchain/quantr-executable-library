package hk.quantr.executablelibrary;

import hk.quantr.executablelibrary.pe.PE;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.InputStream;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestPeWrite {

	@Test
	public void testWrite() {
		try {

			File file = new File("out.exe");
			InputStream inputStream = TestPeRead.class.getResourceAsStream("main.exe");

			RandomAccessFile fo = new RandomAccessFile(file, "rw");
			PE pe = new PE(inputStream);
			//byte data[] = new byte[]{0, 1, 2, 3};
			//byte text[] = new byte[]{0, 1, 2, 3};
			//PE pe = new PE(data, text);

			System.out.println(pe);
			//pe.writeSections.add(new PEWriteSection(".text", new byte[]{(byte) 0xb8, 0x01, 0x00, 0x00, 0x00, (byte) 0xbf, 0x01, 0x00, 0x00, 0x00, 0x48, (byte) 0xbe, (byte) 0xd8, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, (byte) 0xba, 0x0d, 0x00, 0x00, 0x00, 0x0f, 0x05, (byte) 0xb8, 0x3c, 0x00, 0x00, 0x00, 0x48, 0x31, (byte) 0xff, 0x0f, 0x05}));
			//pe.writeSections.add(new PEWriteSection(".data", new byte[]{1, 2, 3}));
			//pe.writeSections.add(new PEWriteSection(".shstrtab", new byte[]{4, 5, 6}));
			pe.write(fo);
			fo.close();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(TestPeRead.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(TestPeWrite.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception ex) {
			Logger.getLogger(TestElfRead.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
