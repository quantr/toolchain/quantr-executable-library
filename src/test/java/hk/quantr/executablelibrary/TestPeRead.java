package hk.quantr.executablelibrary;

import hk.quantr.executablelibrary.pe.PE;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestPeRead {

	@Test
	public void testRead() {
		try {
			InputStream inputStream = TestPeRead.class.getResourceAsStream("main.exe");
			PE dos = new PE(inputStream);

			System.out.println(dos);
		} catch (Exception ex) {
			Logger.getLogger(TestElfRead.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
