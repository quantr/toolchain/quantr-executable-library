package hk.quantr.executablelibrary;

import hk.quantr.executablelibrary.coff.Coff;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestCoffRead {

	@Test
	public void testRead() {
		try {
			InputStream inputStream = TestCoffRead.class.getResourceAsStream("helloworld.obj");
			Coff coff = new Coff(inputStream);
			System.out.println(coff);
		} catch (Exception ex) {
			Logger.getLogger(TestCoffRead.class.getName()).log(Level.SEVERE, null, ex);
		}

	}
}
