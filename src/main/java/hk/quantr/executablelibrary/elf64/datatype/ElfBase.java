/*
 * Copyright 2019 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.elf64.datatype;

import hk.quantr.javalib.CommonLib;
import java.io.InputStream;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ElfBase {

	public byte[] bytes;

	public void read(InputStream inputStream) throws Exception {
		inputStream.read(bytes);
	}

	public String toString() {
		String s = "";
		boolean bingo = false;
		for (int x = bytes.length - 1; x >= 0; x--) {
			if (bingo || bytes[x] != 0) {
				bingo = true;
				s += String.format("%02X", bytes[x]);
			}
		}
		if (!bingo) {
			s += "00";
		}
		return s;
	}

	public long getInt() {
		return CommonLib.getInt(bytes);
	}
}
