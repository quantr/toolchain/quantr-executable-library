/*
 * Copyright 2019 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.elf64.datatype;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Elf64_Byte extends ElfBase {

	public int size = 1;
	public int alignment = 1;

	public Elf64_Byte() {
		bytes = new byte[size];
	}
}
