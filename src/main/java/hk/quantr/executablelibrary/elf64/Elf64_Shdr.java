package hk.quantr.executablelibrary.elf64;

import hk.quantr.executablelibrary.elf64.datatype.Elf64_Addr;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Off;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Word;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Xword;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Elf64_Shdr {

	public Elf64_Word sh_name = new Elf64_Word();
	public Elf64_Word sh_type = new Elf64_Word();
	public Elf64_Xword sh_flags = new Elf64_Xword();
	public Elf64_Addr sh_addr = new Elf64_Addr();
	public Elf64_Off sh_offset = new Elf64_Off();
	public Elf64_Xword sh_size = new Elf64_Xword();
	public Elf64_Word sh_link = new Elf64_Word();
	public Elf64_Word sh_info = new Elf64_Word();
	public Elf64_Xword sh_addralign = new Elf64_Xword();
	public Elf64_Xword sh_entsize = new Elf64_Xword();

	public static byte SHT_NULL = 0; // Marks an unused section header
	public static byte SHT_PROGBITS = 1; // Contains information defined by the program
	public static byte SHT_SYMTAB = 2; // Contains a linker symbol table
	public static byte SHT_STRTAB = 3; // Contains a string table
	public static byte SHT_RELA = 4; // Contains “Rela” type relocation entries
	public static byte SHT_HASH = 5; // Contains a symbol hash table
	public static byte SHT_DYNAMIC = 6; // Contains dynamic linking tables
	public static byte SHT_NOTE = 7; // Contains note information
	public static byte SHT_NOBITS = 8; // Contains uninitialized space; does not occupy any space in the file
	public static byte SHT_REL = 9; // Contains “Rel” type relocation entries
	public static byte SHT_SHLIB = 10; // Reserved
	public static byte SHT_DYNSYM = 11; // Contains a dynamic loader symbol table

	public static byte SHF_WRITE = 0x1; // Section contains writable data
	public static byte SHF_ALLOC = 0x2; // Section is allocated in memory image of program
	public static byte SHF_EXECINSTR = 0x4; // Section contains executable instructions

	void read(InputStream inputStream) throws Exception {
		sh_name.read(inputStream);
		sh_type.read(inputStream);
		sh_flags.read(inputStream);
		sh_addr.read(inputStream);
		sh_offset.read(inputStream);
		sh_size.read(inputStream);
		sh_link.read(inputStream);
		sh_info.read(inputStream);
		sh_addralign.read(inputStream);
		sh_entsize.read(inputStream);
	}

	public String toString() {
		String s = "";
		s += String.format("%-20s %s %n", "sh_name", sh_name);
		s += String.format("%-20s %s %n", "sh_type", sh_type);
		s += String.format("%-20s %s %n", "sh_flags", sh_flags);
		s += String.format("%-20s %s %n", "sh_addr", sh_addr);
		s += String.format("%-20s %s %n", "sh_offset", sh_offset);
		s += String.format("%-20s %s %n", "sh_size", sh_size);
		s += String.format("%-20s %s %n", "sh_link", sh_link);
		s += String.format("%-20s %s %n", "sh_info", sh_info);
		s += String.format("%-20s %s %n", "sh_addralign", sh_addralign);
		s += String.format("%-20s %s %n", "sh_entsize", sh_entsize);

		return s;
	}

	void write(RandomAccessFile fo, HashMap<String, Long> secondPassWrite, String prefix) throws IOException {
		fo.write(sh_name.bytes);
		fo.write(sh_type.bytes);
		fo.write(sh_flags.bytes);
		fo.write(sh_addr.bytes);
		secondPassWrite.put(prefix + "_offset", fo.getFilePointer());
		fo.write(sh_offset.bytes);
		secondPassWrite.put(prefix + "_size", fo.getFilePointer());
		fo.write(sh_size.bytes);
		fo.write(sh_link.bytes);
		fo.write(sh_info.bytes);
		fo.write(sh_addralign.bytes);
		secondPassWrite.put(prefix + "_entsize", fo.getFilePointer());
		fo.write(sh_entsize.bytes);
	}

	public static int size() {
		Elf64_Shdr temp = new Elf64_Shdr();
		return temp.sh_name.size
				+ temp.sh_type.size
				+ temp.sh_flags.size
				+ temp.sh_addr.size
				+ temp.sh_offset.size
				+ temp.sh_size.size
				+ temp.sh_link.size
				+ temp.sh_info.size
				+ temp.sh_addralign.size
				+ temp.sh_entsize.size;
	}
}
