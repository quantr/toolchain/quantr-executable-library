package hk.quantr.executablelibrary.elf64;

import hk.quantr.executablelibrary.elf64.datatype.Elf64_Addr;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Off;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Word;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Xword;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Elf64_Phdr {

	public Elf64_Word p_type = new Elf64_Word();
	public Elf64_Word p_flags = new Elf64_Word();
	public Elf64_Off p_offset = new Elf64_Off();
	public Elf64_Addr p_vaddr = new Elf64_Addr();
	public Elf64_Addr p_paddr = new Elf64_Addr();
	public Elf64_Xword p_filesz = new Elf64_Xword();
	public Elf64_Xword p_memsz = new Elf64_Xword();
	public Elf64_Xword p_align = new Elf64_Xword();

	public static byte PF_X = 0x1; // Execute permission
	public static byte PF_W = 0x2; // Write permission
	public static byte PF_R = 0x4; // Read permission

	public static byte PT_NULL = 0; // Unused entry
	public static byte PT_LOAD = 1; // Loadable segment
	public static byte PT_DYNAMIC = 2; // Dynamic linking tables
	public static byte PT_INTERP = 3; // Program interpreter path name
	public static byte PT_NOTE = 4; // Note sections
	public static byte PT_SHLIB = 5; // Reserved
	public static byte PT_PHDR = 6; // Program header table

	void read(InputStream inputStream) throws Exception {
		p_type.read(inputStream);
		p_flags.read(inputStream);
		p_offset.read(inputStream);
		p_vaddr.read(inputStream);
		p_paddr.read(inputStream);
		p_filesz.read(inputStream);
		p_memsz.read(inputStream);
		p_align.read(inputStream);
	}

	public String toString() {
		String s = "";
		s += String.format("%-20s %s %n", "p_type", p_type);
		s += String.format("%-20s %s %n", "p_flags", p_flags);
		s += String.format("%-20s %s %n", "p_offset", p_offset);
		s += String.format("%-20s %s %n", "p_vaddr", p_vaddr);
		s += String.format("%-20s %s %n", "p_paddr", p_paddr);
		s += String.format("%-20s %s %n", "p_filesz", p_filesz);
		s += String.format("%-20s %s %n", "p_memsz", p_memsz);
		s += String.format("%-20s %s %n", "p_align", p_align);

		return s;
	}

	void write(RandomAccessFile fo, HashMap<String, Long> secondPassWrite, String prefix) throws IOException {
		fo.write(p_type.bytes);
		fo.write(p_flags.bytes);
		secondPassWrite.put(prefix + "_p_offset", fo.getFilePointer());
		fo.write(p_offset.bytes);
		fo.write(p_vaddr.bytes);
		fo.write(p_paddr.bytes);
		fo.write(p_filesz.bytes);
		fo.write(p_memsz.bytes);
		fo.write(p_align.bytes);
	}

	public static int size() {
		Elf64_Phdr temp = new Elf64_Phdr();
		return temp.p_type.size
				+ temp.p_flags.size
				+ temp.p_offset.size
				+ temp.p_vaddr.size
				+ temp.p_paddr.size
				+ temp.p_filesz.size
				+ temp.p_memsz.size
				+ temp.p_align.size;
	}

}
