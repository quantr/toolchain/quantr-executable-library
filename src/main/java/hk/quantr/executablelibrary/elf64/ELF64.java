package hk.quantr.executablelibrary.elf64;

import java.io.InputStream;
import java.util.ArrayList;

import hk.quantr.javalib.CommonLib;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Addr;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Half;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Off;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Word;
import hk.quantr.executablelibrary.elf64.datatype.Elf64_Xword;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ELF64 implements Serializable {

	public Elf64_Ehdr elf64_Ehdr;
	public ArrayList<Elf64_Phdr> elf64_Phdrs = new ArrayList<>();
	public ArrayList<Elf64_Shdr> elf64_Shdrs = new ArrayList<>();
	String[] sections = {".bss", ".comment", ".data", ".data1", ".debug", ".dynamic", ".dynstr", ".dynsym", ".fini", ".fini_array", ".got", ".hash", ".init", ".init_array", ".interp", ".line", ".note", ".plt", ".preinit_array", ".relname", ".relaname", ".rodata", ".rodata", ".shstrtab", ".strtab", ".symtab", ".symtab_shndx", ".tbss", ".tdata", ".tdata1", ".text"};

	public HashMap<String, Integer> sectionOffset = new HashMap();

	public HashMap<String, Long> secondPassWrite = new HashMap();
	public ArrayList<ELF64WriteSection> writeSections = new ArrayList();

	public ELF64() {
	}

	public ELF64(InputStream inputStream) throws Exception {
		elf64_Ehdr = new Elf64_Ehdr();
		read(inputStream);
	}

	public void read(InputStream inputStream) throws Exception {
		byte bytes[] = inputStream.readAllBytes();
		inputStream = new ByteArrayInputStream(bytes);
		elf64_Ehdr.read(inputStream);
		if (elf64_Ehdr.e_ident[Elf64_Ehdr.EI_CLASS] != 2) {
			System.out.println("This library only supports 64 bits ELF");
			throw new Exception();
		}

		inputStream = new ByteArrayInputStream(bytes);
		CommonLib.skip(inputStream, elf64_Ehdr.e_shoff.getInt());
		for (int x = 0; x < elf64_Ehdr.e_shnum.getInt(); x++) {
			Elf64_Shdr elf64_Shdr = new Elf64_Shdr();
			elf64_Shdr.read(inputStream);
			elf64_Shdrs.add(elf64_Shdr);
		}

		inputStream = new ByteArrayInputStream(bytes);
		CommonLib.skip(inputStream, elf64_Ehdr.e_phoff.getInt());
		for (int x = 0; x < elf64_Ehdr.e_phnum.getInt(); x++) {
			Elf64_Phdr elf64_Phdr = new Elf64_Phdr();
			elf64_Phdr.read(inputStream);
			elf64_Phdrs.add(elf64_Phdr);
		}
	}

//	public void write(ELF64 elf, File file) throws Exception {
//		FileOutputStream fo = new FileOutputStream(file);
//		fo.close();
//	}
	public static byte[] getByteArray_half(int value) {
		byte b[] = new byte[]{(byte) (value & 0xff), (byte) (value >> 8 & 0xff)};
//		for (int x = 0; x < 8; x++) {
//			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
//		}
		return b;
	}

	public static byte[] getByteArray_word(int value) {
		byte b[] = new byte[4];
		for (int x = 0; x < 4; x++) {
			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}

	public static byte[] getByteArray_xword(int val) {
		long value = val;
		byte b[] = new byte[8];
		for (int x = 0; x < 8; x++) {
//			System.out.println(">> " + (8 - x - 1) + " > " + ((byte) ((value >> (x * 8)) & 0xffL)));
			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}

	public static byte[] getByteArray_xword(long value) {
		byte b[] = new byte[8];
		for (int x = 0; x < 8; x++) {
//			System.out.println(">> " + (8 - x - 1) + " > " + ((byte) ((value >> (x * 8)) & 0xffL)));
			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}

	@Override
	public String toString() {
		String s = "";
		s += elf64_Ehdr + "\n";
		for (Elf64_Shdr elf32_Shdr : elf64_Shdrs) {
			s += elf32_Shdr + "\n";
		}
		s += "------------------------------------\n";
		for (Elf64_Phdr elf64_Phdr : elf64_Phdrs) {
			s += elf64_Phdr + "\n";
		}
		return s;
	}

	public void secondWrite(RandomAccessFile fo, String anhor, byte[] bytes) throws IOException {
		if (secondPassWrite.get(anhor) == null) {
			System.out.println(anhor + " not exist");
			System.exit(2);
		}
		long position = fo.getFilePointer();
		fo.seek(secondPassWrite.get(anhor));
		fo.write(bytes);
		fo.seek(position);
	}

	public void write(RandomAccessFile fo) throws FileNotFoundException, IOException {

		String[] sections = {".bss", ".comment", ".data", ".data1", ".debug", ".dynamic", ".dynstr", ".dynsym", ".fini", ".fini_array", ".got", ".hash", ".init", ".init_array", ".interp", ".line", ".note", ".plt", ".preinit_array", ".relname", ".relaname", ".rodata", ".rodata", ".shstrtab", ".strtab", ".symtab", ".symtab_shndx", ".tbss", ".tdata", ".tdata1", ".text"};

		elf64_Ehdr = new Elf64_Ehdr();
		elf64_Ehdr.e_ident = new byte[]{0x7F, 0x45, 0x4C, 0x46, 0x02, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
		elf64_Ehdr.e_type = new Elf64_Half(new byte[]{0x2, 0x0});
		elf64_Ehdr.e_machine = new Elf64_Half(new byte[]{(byte) 243, 0x0});
		elf64_Ehdr.e_version = new Elf64_Word(new byte[]{0x1, 0x0, 0x0, 0x0});
		elf64_Ehdr.e_entry = new Elf64_Addr(ELF64.getByteArray_xword(0x4000b0));
		elf64_Ehdr.e_phoff = new Elf64_Off(ELF64.getByteArray_xword(0));
		elf64_Ehdr.e_shoff = new Elf64_Off(ELF64.getByteArray_xword(0x0));
		elf64_Ehdr.e_flags = new Elf64_Word(new byte[]{0x0, 0x0, 0x0, 0x0});
		elf64_Ehdr.e_ehsize = new Elf64_Half(new byte[]{0x40, 0x0});
		elf64_Ehdr.e_phentsize = new Elf64_Half(new byte[]{0x38, 0x0});
		elf64_Ehdr.e_phnum = new Elf64_Half(ELF64.getByteArray_half(elf64_Phdrs.size()));
		elf64_Ehdr.e_shentsize = new Elf64_Half(new byte[]{0x40, 0x0});
		elf64_Ehdr.e_shnum = new Elf64_Half(ELF64.getByteArray_half(elf64_Shdrs.size()));
		elf64_Ehdr.e_shstrndx = new Elf64_Half(new byte[]{0x3, 0x0});
		elf64_Ehdr.write(fo, secondPassWrite);

		// program headers
		secondWrite(fo, "e_phoff", ELF64.getByteArray_xword(fo.getFilePointer()));

		
		
		for (ELF64WriteSection i : writeSections) {
			if (Arrays.asList(sections).contains(i.name)) {
				Elf64_Phdr elf64_Phdr = new Elf64_Phdr();
				elf64_Phdr.p_type = new Elf64_Word(ELF64.getByteArray_word(Elf64_Phdr.PT_LOAD));
				elf64_Phdr.p_offset = new Elf64_Off(ELF64.getByteArray_xword(0));
				elf64_Phdr.p_vaddr = new Elf64_Addr(ELF64.getByteArray_xword(0x0000000000400000));
				elf64_Phdr.p_paddr = new Elf64_Addr(ELF64.getByteArray_xword(0x0000000000400000));
				elf64_Phdr.p_filesz = new Elf64_Xword(ELF64.getByteArray_xword(getWriteSection(i.name).bytes.length));
				elf64_Phdr.p_memsz = new Elf64_Xword(ELF64.getByteArray_xword(getWriteSection(i.name).bytes.length));
				elf64_Phdr.p_align = new Elf64_Xword(ELF64.getByteArray_xword(0x200000));
				elf64_Phdr.p_flags = new Elf64_Word(ELF64.getByteArray_word(Elf64_Phdr.PF_R | Elf64_Phdr.PF_X));
				elf64_Phdr.write(fo, secondPassWrite, i.name);
			}
		}

//		Elf64_Phdr elf64_Phdr = new Elf64_Phdr();
//		elf64_Phdr.p_type = new Elf64_Word(ELF64.getByteArray_word(Elf64_Phdr.PT_LOAD));
//		elf64_Phdr.p_offset = new Elf64_Off(ELF64.getByteArray_xword(0));
//		elf64_Phdr.p_vaddr = new Elf64_Addr(ELF64.getByteArray_xword(0x0000000000400000));
//		elf64_Phdr.p_paddr = new Elf64_Addr(ELF64.getByteArray_xword(0x0000000000400000));
//		elf64_Phdr.p_filesz = new Elf64_Xword(ELF64.getByteArray_xword(getWriteSection(".text").bytes.length));
//		elf64_Phdr.p_memsz = new Elf64_Xword(ELF64.getByteArray_xword(getWriteSection(".text").bytes.length));
//		elf64_Phdr.p_align = new Elf64_Xword(ELF64.getByteArray_xword(0x200000));
//		elf64_Phdr.p_flags = new Elf64_Word(ELF64.getByteArray_word(Elf64_Phdr.PF_R | Elf64_Phdr.PF_X));
//		elf64_Phdr.write(fo, secondPassWrite, ".text");
//
//		elf64_Phdr = new Elf64_Phdr();
//		elf64_Phdr.p_type = new Elf64_Word(ELF64.getByteArray_word(Elf64_Phdr.PT_LOAD));
//		elf64_Phdr.p_offset = new Elf64_Off(ELF64.getByteArray_xword(0));
//		elf64_Phdr.p_vaddr = new Elf64_Addr(ELF64.getByteArray_xword(0x00000000006000d8));
//		elf64_Phdr.p_paddr = new Elf64_Addr(ELF64.getByteArray_xword(0x00000000006000d8));
//		elf64_Phdr.p_filesz = new Elf64_Xword(ELF64.getByteArray_xword(getWriteSection(".data").bytes.length));
//		elf64_Phdr.p_memsz = new Elf64_Xword(ELF64.getByteArray_xword(getWriteSection(".data").bytes.length));
//		elf64_Phdr.p_align = new Elf64_Xword(ELF64.getByteArray_xword(0x200000));
//		elf64_Phdr.p_flags = new Elf64_Word(ELF64.getByteArray_word(Elf64_Phdr.PF_R | Elf64_Phdr.PF_W));
//		elf64_Phdr.write(fo, secondPassWrite, ".data");

		secondWrite(fo, "e_phnum", ELF64.getByteArray_half(2));

		// sections
//		String sections[] = {".shstrtab", ".text", ".data"};

		secondWrite(fo, "e_shoff", ELF64.getByteArray_xword(fo.getFilePointer()));

		Elf64_Shdr elf64_Shdr = new Elf64_Shdr();
		elf64_Shdr.sh_name = new Elf64_Word(ELF64.getByteArray_word(0));
		elf64_Shdr.sh_type = new Elf64_Word(ELF64.getByteArray_word(Elf64_Shdr.SHT_NULL));
		elf64_Shdr.sh_addr = new Elf64_Addr(ELF64.getByteArray_xword(0000000000000000));
		elf64_Shdr.sh_offset = new Elf64_Off(ELF64.getByteArray_xword(00000000));
		elf64_Shdr.sh_size = new Elf64_Xword(ELF64.getByteArray_xword(0000000000000000));
		elf64_Shdr.sh_entsize = new Elf64_Xword(ELF64.getByteArray_xword(0000000000000000));
		elf64_Shdr.sh_flags = new Elf64_Xword(ELF64.getByteArray_xword(0));
		elf64_Shdr.sh_link = new Elf64_Word(ELF64.getByteArray_word(0));
		elf64_Shdr.sh_info = new Elf64_Word(ELF64.getByteArray_word(0));
		elf64_Shdr.sh_addralign = new Elf64_Xword(ELF64.getByteArray_xword(0));
		elf64_Shdr.write(fo, secondPassWrite, "");
		
		for (ELF64WriteSection i : writeSections) {
			if (Arrays.asList(sections).contains(i.name)) {
				System.out.println(i.name);
				elf64_Shdr = new Elf64_Shdr();
				elf64_Shdr.sh_name = new Elf64_Word(ELF64.getByteArray_word(ELF64.getStringOffset(sections, i.name)));
				elf64_Shdr.sh_type = new Elf64_Word(ELF64.getByteArray_word(Elf64_Shdr.SHT_PROGBITS));
				elf64_Shdr.sh_addr = new Elf64_Addr(ELF64.getByteArray_xword(0x0000000000400000));
				elf64_Shdr.sh_offset = new Elf64_Off(ELF64.getByteArray_xword(0));
				elf64_Shdr.sh_size = new Elf64_Xword(ELF64.getByteArray_xword(0));
				elf64_Shdr.sh_entsize = new Elf64_Xword(ELF64.getByteArray_xword(0000000000000000));
				elf64_Shdr.sh_flags = new Elf64_Xword(ELF64.getByteArray_xword(Elf64_Shdr.SHF_ALLOC | Elf64_Shdr.SHF_EXECINSTR));
				elf64_Shdr.sh_link = new Elf64_Word(ELF64.getByteArray_word(0));
				elf64_Shdr.sh_info = new Elf64_Word(ELF64.getByteArray_word(0));
				elf64_Shdr.sh_addralign = new Elf64_Xword(ELF64.getByteArray_xword(16));
				elf64_Shdr.write(fo, secondPassWrite, i.name);
			}
		}

//		elf64_Shdr = new Elf64_Shdr();
//		elf64_Shdr.sh_name = new Elf64_Word(ELF64.getByteArray_word(ELF64.getStringOffset(sections, ".text")));
//		elf64_Shdr.sh_type = new Elf64_Word(ELF64.getByteArray_word(Elf64_Shdr.SHT_PROGBITS));
//		elf64_Shdr.sh_addr = new Elf64_Addr(ELF64.getByteArray_xword(0x0000000000400000));
//		elf64_Shdr.sh_offset = new Elf64_Off(ELF64.getByteArray_xword(0));
//		elf64_Shdr.sh_size = new Elf64_Xword(ELF64.getByteArray_xword(0));
//		elf64_Shdr.sh_entsize = new Elf64_Xword(ELF64.getByteArray_xword(0000000000000000));
//		elf64_Shdr.sh_flags = new Elf64_Xword(ELF64.getByteArray_xword(Elf64_Shdr.SHF_ALLOC | Elf64_Shdr.SHF_EXECINSTR));
//		elf64_Shdr.sh_link = new Elf64_Word(ELF64.getByteArray_word(0));
//		elf64_Shdr.sh_info = new Elf64_Word(ELF64.getByteArray_word(0));
//		elf64_Shdr.sh_addralign = new Elf64_Xword(ELF64.getByteArray_xword(16));
//		elf64_Shdr.write(fo, secondPassWrite, ".text");
//
//		elf64_Shdr = new Elf64_Shdr();
//		elf64_Shdr.sh_name = new Elf64_Word(ELF64.getByteArray_word(ELF64.getStringOffset(sections, ".data")));
//		elf64_Shdr.sh_type = new Elf64_Word(ELF64.getByteArray_word(Elf64_Shdr.SHT_PROGBITS));
//		elf64_Shdr.sh_addr = new Elf64_Addr(ELF64.getByteArray_xword(0x00000000006000d8));
//		elf64_Shdr.sh_offset = new Elf64_Off(ELF64.getByteArray_xword(0));
//		elf64_Shdr.sh_size = new Elf64_Xword(ELF64.getByteArray_xword(0));
//		elf64_Shdr.sh_entsize = new Elf64_Xword(ELF64.getByteArray_xword(0000000000000000));
//		elf64_Shdr.sh_flags = new Elf64_Xword(ELF64.getByteArray_xword(Elf64_Shdr.SHF_ALLOC | Elf64_Shdr.SHF_WRITE));
//		elf64_Shdr.sh_link = new Elf64_Word(ELF64.getByteArray_word(0));
//		elf64_Shdr.sh_info = new Elf64_Word(ELF64.getByteArray_word(0));
//		elf64_Shdr.sh_addralign = new Elf64_Xword(ELF64.getByteArray_xword(4));
//		elf64_Shdr.write(fo, secondPassWrite, ".data");
//
//		elf64_Shdr = new Elf64_Shdr();
//		elf64_Shdr.sh_name = new Elf64_Word(ELF64.getByteArray_word(ELF64.getStringOffset(sections, ".shstrtab")));
//		elf64_Shdr.sh_type = new Elf64_Word(ELF64.getByteArray_word(Elf64_Shdr.SHT_STRTAB));
//		elf64_Shdr.sh_addr = new Elf64_Addr(ELF64.getByteArray_xword(0000000000000000));
//		elf64_Shdr.sh_offset = new Elf64_Off(ELF64.getByteArray_xword(0));
//		elf64_Shdr.sh_size = new Elf64_Xword(ELF64.getByteArray_xword(0));
//		elf64_Shdr.sh_entsize = new Elf64_Xword(ELF64.getByteArray_xword(0000000000000000));
//		elf64_Shdr.sh_flags = new Elf64_Xword(ELF64.getByteArray_xword(0));
//		elf64_Shdr.sh_link = new Elf64_Word(ELF64.getByteArray_word(0));
//		elf64_Shdr.sh_info = new Elf64_Word(ELF64.getByteArray_word(0));
//		elf64_Shdr.sh_addralign = new Elf64_Xword(ELF64.getByteArray_xword(1));
//		elf64_Shdr.write(fo, secondPassWrite, ".shstrtab");
//		secondWrite(fo, "e_shnum", ELF64.getByteArray_half(4));

		for (ELF64WriteSection i : writeSections) {
			if (Arrays.asList(sections).contains(i.name)) {
				System.out.println("t=" + fo.getFilePointer());
				secondWrite(fo, ".text_offset", ELF64.getByteArray_xword(fo.getFilePointer()));
				secondWrite(fo, ".text_p_offset", ELF64.getByteArray_xword(fo.getFilePointer()));
				fo.write(getWriteSection(i.name).bytes);
				secondWrite(fo, ".text_size", ELF64.getByteArray_xword(getWriteSection(i.name).bytes.length));
				secondWrite(fo, ".text_entsize", ELF64.getByteArray_xword(getWriteSection(i.name).bytes.length));
			}
		}

//		// .text
//		System.out.println("t=" + fo.getFilePointer());
//		secondWrite(fo, ".text_offset", ELF64.getByteArray_xword(fo.getFilePointer()));
//		secondWrite(fo, ".text_p_offset", ELF64.getByteArray_xword(fo.getFilePointer()));
//		fo.write(getWriteSection(".text").bytes);
//		secondWrite(fo, ".text_size", ELF64.getByteArray_xword(getWriteSection(".text").bytes.length));
//		secondWrite(fo, ".text_entsize", ELF64.getByteArray_xword(getWriteSection(".text").bytes.length));
////		 .data
//		System.out.println("d=" + fo.getFilePointer());
//		secondWrite(fo, ".data_offset", ELF64.getByteArray_xword(fo.getFilePointer()));
//		secondWrite(fo, ".data_p_offset", ELF64.getByteArray_xword(fo.getFilePointer()));
//		fo.write(getWriteSection(".data").bytes);
//		secondWrite(fo, ".data_size", ELF64.getByteArray_xword(getWriteSection(".data").bytes.length));
//		secondWrite(fo, ".data_entsize", ELF64.getByteArray_xword(getWriteSection(".data").bytes.length));
////		 shstrtab
//		secondWrite(fo, ".shstrtab_offset", ELF64.getByteArray_xword(fo.getFilePointer()));
//		long begin = fo.getFilePointer();
//		fo.write(0);
//		for (String s : sections) {
//			fo.write(s.getBytes());
//			fo.write(0);
//		}
//		long end = fo.getFilePointer();
//		secondWrite(fo, ".shstrtab_size", ELF64.getByteArray_xword(end - begin));
	}

	public ELF64WriteSection getWriteSection(String sectionName) {
		for (ELF64WriteSection section : writeSections) {
			if (section.name.equals(sectionName)) {
				return section;
			}
		}
		return null;
	}

	public static int getStringOffset(String[] str, String s) {
		int x = 1;
		for (String temp : str) {
			if (s.equals(temp)) {
				return x;
			}
			x += temp.length() + 1;
		}
		return 0;
	}

}
