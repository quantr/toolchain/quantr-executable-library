package hk.quantr.executablelibrary.elf32.datatype;

/*
 * Copyright 2020 user.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author user
 */
public class Elf32_Addr  extends ElfBase{
	public int size = 4;
	public int alignment = 4;

	public Elf32_Addr() {
		bytes = new byte[size];
	}

	public Elf32_Addr(byte[] bytes) {
		this.bytes = bytes;
	}
}
