/*
 * Copyright 2020 user.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.elf32;

import hk.quantr.executablelibrary.elf32.datatype.Elf32_Addr;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Off;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Word;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

/**
 *
 * @author user
 */
public class Elf32_Phdr {
	public Elf32_Word p_type = new Elf32_Word();
	public Elf32_Off p_offset = new Elf32_Off();
	public Elf32_Addr p_vaddr = new Elf32_Addr();
	public Elf32_Addr p_paddr = new Elf32_Addr();
	public Elf32_Word p_filesz = new Elf32_Word();
	public Elf32_Word p_memsz = new Elf32_Word();
	public Elf32_Word p_flags = new Elf32_Word();
	public Elf32_Word p_align = new Elf32_Word();
	
	public static byte PF_X = 0x1; // Execute permission
	public static byte PF_W = 0x2; // Write permission
	public static byte PF_R = 0x4; // Read permission
	
	public static byte PT_NULL = 0; // Unused entry
	public static byte PT_LOAD = 1; // Loadable segment
	public static byte PT_DYNAMIC = 2; // Dynamic linking tables
	public static byte PT_INTERP = 3; // Program interpreter path name
	public static byte PT_NOTE = 4; // Note sections
	public static byte PT_SHLIB = 5; // Reserved
	public static byte PT_PHDR = 6; // Program header table
	
	void read(InputStream inputStream) throws Exception {
		p_type.read(inputStream);
		p_flags.read(inputStream);
		p_offset.read(inputStream);
		p_vaddr.read(inputStream);
		p_paddr.read(inputStream);
		p_filesz.read(inputStream);
		p_memsz.read(inputStream);
		p_align.read(inputStream);
	}
	
	public String toString() {
		String s = "";
		s += String.format("%-20s %s %n", "p_type", p_type);
		s += String.format("%-20s %s %n", "p_flags", p_flags);
		s += String.format("%-20s %s %n", "p_offset", p_offset);
		s += String.format("%-20s %s %n", "p_vaddr", p_vaddr);
		s += String.format("%-20s %s %n", "p_paddr", p_paddr);
		s += String.format("%-20s %s %n", "p_filesz", p_filesz);
		s += String.format("%-20s %s %n", "p_memsz", p_memsz);
		s += String.format("%-20s %s %n", "p_align", p_align);

		return s;
	}
	
	void write(RandomAccessFile fo) throws IOException {
		fo.write(p_type.bytes);
		fo.write(p_offset.bytes);
		fo.write(p_vaddr.bytes);
		fo.write(p_paddr.bytes);
		fo.write(p_filesz.bytes);
		fo.write(p_memsz.bytes);
		fo.write(p_flags.bytes);
		fo.write(p_align.bytes);
	}
	
	public static int size() {
		Elf32_Phdr temp = new Elf32_Phdr();
		return temp.p_type.size
				+ temp.p_flags.size
				+ temp.p_offset.size
				+ temp.p_vaddr.size
				+ temp.p_paddr.size
				+ temp.p_filesz.size
				+ temp.p_memsz.size
				+ temp.p_align.size;
	}
}
