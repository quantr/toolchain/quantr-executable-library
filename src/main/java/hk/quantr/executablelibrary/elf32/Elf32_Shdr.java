/*
 * Copyright 2020 user.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.elf32;

import hk.quantr.executablelibrary.elf32.datatype.Elf32_Addr;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Off;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Word;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;

/**
 *
 * @author user
 */
public class Elf32_Shdr {
	public Elf32_Word sh_name = new Elf32_Word();
	public Elf32_Word sh_type = new Elf32_Word();
	public Elf32_Word sh_flags = new Elf32_Word();
	public Elf32_Addr sh_addr = new Elf32_Addr();
	public Elf32_Off sh_offset = new Elf32_Off();
	public Elf32_Word sh_size = new Elf32_Word();
	public Elf32_Word sh_link = new Elf32_Word();
	public Elf32_Word sh_info = new Elf32_Word();
	public Elf32_Word sh_addralign = new Elf32_Word();
	public Elf32_Word sh_entsize = new Elf32_Word();
	
	public static byte SHT_NULL = 0; // Marks an unused section header
	public static byte SHT_PROGBITS = 1; // Contains information defined by the program
	public static byte SHT_SYMTAB = 2; // Contains a linker symbol table
	public static byte SHT_STRTAB = 3; // Contains a string table
	public static byte SHT_RELA = 4; // Contains “Rela” type relocation entries
	public static byte SHT_HASH = 5; // Contains a symbol hash table
	public static byte SHT_DYNAMIC = 6; // Contains dynamic linking tables
	public static byte SHT_NOTE = 7; // Contains note information
	public static byte SHT_NOBITS = 8; // Contains uninitialized space; does not occupy any space in the file
	public static byte SHT_REL = 9; // Contains “Rel” type relocation entries
	public static byte SHT_SHLIB = 10; // Reserved
	public static byte SHT_DYNSYM = 11; // Contains a dynamic loader symbol table
	public static byte SHT_LOPROC =  (byte) 0x70000000; // e reserved for processor-specific semantics
	
	public static byte SHF_WRITE = 0x1; // Section contains writable data
	public static byte SHF_ALLOC = 0x2; // Section is allocated in memory image of program
	public static byte SHF_EXECINSTR = 0x4; // Section contains executable instructions
	
	void read(InputStream inputStream) throws Exception {
		sh_name.read(inputStream);
		sh_type.read(inputStream);
		sh_flags.read(inputStream);
		sh_addr.read(inputStream);
		sh_offset.read(inputStream);
		sh_size.read(inputStream);
		sh_link.read(inputStream);
		sh_info.read(inputStream);
		sh_addralign.read(inputStream);
		sh_entsize.read(inputStream);
	}
	
	public String toString() {
		String s = "";
		s += String.format("%-20s %s %n", "sh_name", sh_name);
		s += String.format("%-20s %s %n", "sh_type", sh_type);
		s += String.format("%-20s %s %n", "sh_flags", sh_flags);
		s += String.format("%-20s %s %n", "sh_addr", sh_addr);
		s += String.format("%-20s %s %n", "sh_offset", sh_offset);
		s += String.format("%-20s %s %n", "sh_size", sh_size);
		s += String.format("%-20s %s %n", "sh_link", sh_link);
		s += String.format("%-20s %s %n", "sh_info", sh_info);
		s += String.format("%-20s %s %n", "sh_addralign", sh_addralign);
		s += String.format("%-20s %s %n", "sh_entsize", sh_entsize);

		return s;
	}
	
	void write(RandomAccessFile fo, HashMap<String, Long> secondPassWrite, String prefix) throws IOException {
		fo.write(sh_name.bytes);
		fo.write(sh_type.bytes);
		fo.write(sh_flags.bytes);
		fo.write(sh_addr.bytes);
		secondPassWrite.put(prefix + "_offset", fo.getFilePointer());
		fo.write(sh_offset.bytes);
		secondPassWrite.put(prefix + "_size", fo.getFilePointer());
		fo.write(sh_size.bytes);
		fo.write(sh_link.bytes);
		fo.write(sh_info.bytes);
		fo.write(sh_addralign.bytes);
		fo.write(sh_entsize.bytes);
	}
	
	public static int size() {
		Elf32_Shdr temp = new Elf32_Shdr();
		return temp.sh_name.size
				+ temp.sh_type.size
				+ temp.sh_flags.size
				+ temp.sh_addr.size
				+ temp.sh_offset.size
				+ temp.sh_size.size
				+ temp.sh_link.size
				+ temp.sh_info.size
				+ temp.sh_addralign.size
				+ temp.sh_entsize.size;
	}
}
