/*
 * Copyright 2020 user.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.elf32;

import hk.quantr.executablelibrary.elf32.datatype.Elf32_Addr;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Half;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Off;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Word;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;

/**
 *
 * @author user
 */
public class Elf32_Ehdr {
	public byte e_ident[] = new byte[EI_NIDENT];

	public static int EI_MAG0 = 0;
	public static int EI_MAG1 = 1;
	public static int EI_MAG2 = 2;
	public static int EI_MAG3 = 3;
	public static int EI_CLASS = 4;
	public static int EI_DATA = 5;
	public static int EI_VERSION = 6;
	public static int EI_PAD = 7;
	public static int EI_NIDENT = 16;
	
	public Elf32_Half e_type = new Elf32_Half();
	public Elf32_Half e_machine = new Elf32_Half();
	public Elf32_Word e_version = new Elf32_Word();
	public Elf32_Addr e_entry = new Elf32_Addr();
	public Elf32_Off e_phoff = new Elf32_Off();
	public Elf32_Off e_shoff = new Elf32_Off();
	public Elf32_Word e_flags = new Elf32_Word();
	public Elf32_Half e_ehsize = new Elf32_Half();
	public Elf32_Half e_phentsize = new Elf32_Half();
	public Elf32_Half e_phnum = new Elf32_Half();
	public Elf32_Half e_shentsize = new Elf32_Half();
	public Elf32_Half e_shnum = new Elf32_Half();
	public Elf32_Half e_shstrndx = new Elf32_Half();
	
	void read(InputStream inputStream) throws Exception {
		inputStream.read(e_ident);

		e_type.read(inputStream);
		e_machine.read(inputStream);
		e_version.read(inputStream);
		e_entry.read(inputStream);
		e_phoff.read(inputStream);
		e_shoff.read(inputStream);
		e_flags.read(inputStream);
		e_ehsize.read(inputStream);
		e_phentsize.read(inputStream);
		e_phnum.read(inputStream);
		e_shentsize.read(inputStream);
		e_shnum.read(inputStream);
		e_shstrndx.read(inputStream);
	}
	
	public String toString() {
		String s = "";
		s += "ELF Header:\n";
		s += String.format("%-20s ", "Magic");
		for (int x = 0; x < EI_NIDENT; x++) {
			s += String.format("%02X", e_ident[x]) + " ";
		}
		s += "\n";

		s += String.format("%-20s %s %n", "Type", e_type);
		s += String.format("%-20s %s %n", "Machine", e_machine);
		s += String.format("%-20s %s %n", "Version", e_version);
		s += String.format("%-20s %s %n", "Entry", e_entry);
		s += String.format("%-20s %s %n", "e_phoff", e_phoff);
		s += String.format("%-20s %s %n", "e_shoff", e_shoff);
		s += String.format("%-20s %s %n", "e_flags", e_flags);
		s += String.format("%-20s %s %n", "e_ehsize", e_ehsize);
		s += String.format("%-20s %s %n", "e_phentsize", e_phentsize);
		s += String.format("%-20s %s %n", "e_phnum", e_phnum);
		s += String.format("%-20s %s %n", "e_shentsize", e_shentsize);
		s += String.format("%-20s %s %n", "e_shnum", e_shnum);
		s += String.format("%-20s %s %n", "e_shstrndx", e_shstrndx);

		return s;
	}
	
	void write(RandomAccessFile fo, HashMap<String, Long> secondPassWrite) throws IOException {
		fo.write(e_ident);
		fo.write(e_type.bytes);
		fo.write(e_machine.bytes);
		fo.write(e_version.bytes);
		fo.write(e_entry.bytes);
		secondPassWrite.put("e_phoff", fo.getFilePointer());
		fo.write(e_phoff.bytes);
		secondPassWrite.put("e_shoff", fo.getFilePointer());
		fo.write(e_shoff.bytes);
		fo.write(e_flags.bytes);
		fo.write(e_ehsize.bytes);
		fo.write(e_phentsize.bytes);
		secondPassWrite.put("e_phnum", fo.getFilePointer());
		fo.write(e_phnum.bytes);
		fo.write(e_shentsize.bytes);
		secondPassWrite.put("e_shnum", fo.getFilePointer());
		fo.write(e_shnum.bytes);
		fo.write(e_shstrndx.bytes);
	}
	
	public static int size() {
		Elf32_Ehdr temp = new Elf32_Ehdr();
		return temp.e_ident.length
				+ temp.e_type.size
				+ temp.e_machine.size
				+ temp.e_version.size
				+ temp.e_entry.size
				+ temp.e_phoff.size
				+ temp.e_shoff.size
				+ temp.e_flags.size
				+ temp.e_ehsize.size
				+ temp.e_phentsize.size
				+ temp.e_phnum.size
				+ temp.e_shentsize.size
				+ temp.e_shnum.size
				+ temp.e_shstrndx.size;
	}

}
