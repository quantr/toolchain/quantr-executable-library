package hk.quantr.executablelibrary.elf32;

import hk.quantr.executablelibrary.elf32.datatype.Elf32_Addr;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Half;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Off;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Word;
import hk.quantr.executablelibrary.elf32.ELF32;
import hk.quantr.executablelibrary.elf32.datatype.Elf32_Half;
import hk.quantr.javalib.CommonLib;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.HashMap;
import java.util.ArrayList;

/*
 * Copyright 2020 user.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author user
 */
public class ELF32 {

	private static byte[] getByteArray_ord(int i) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	public Elf32_Ehdr elf32_Ehdr;
	public ArrayList<Elf32_Phdr> elf32_Phdrs = new ArrayList<Elf32_Phdr>();
	public ArrayList<Elf32_Shdr> elf32_Shdrs = new ArrayList<Elf32_Shdr>();
	public String[] sectionsName;
		
	public HashMap<String, Integer> sectionOffset = new HashMap();

	public HashMap<String, Long> secondPassWrite = new HashMap();
	public ArrayList<ELF32WriteSection> writeSections = new ArrayList();
	
	public ELF32(InputStream inputStream) throws Exception {
		elf32_Ehdr = new Elf32_Ehdr();
		read(inputStream);
	}
	
	public ELF32() {
	}

	
	public void read(InputStream inputStream) throws Exception {
		byte bytes[] = inputStream.readAllBytes();
		inputStream = new ByteArrayInputStream(bytes);
		elf32_Ehdr.read(inputStream);
		if (elf32_Ehdr.e_ident[Elf32_Ehdr.EI_CLASS] != 1) {
			System.out.println("This library only supports 32 bits ELF");
			throw new Exception();
		}
		
		inputStream = new ByteArrayInputStream(bytes);
		CommonLib.skip(inputStream, elf32_Ehdr.e_phoff.getInt());
		for (int x = 0; x < elf32_Ehdr.e_phnum.getInt(); x++) {
			Elf32_Phdr elf32_Phdr = new Elf32_Phdr();
			elf32_Phdr.read(inputStream);
			elf32_Phdrs.add(elf32_Phdr);
		}
		
		inputStream = new ByteArrayInputStream(bytes);
		CommonLib.skip(inputStream, elf32_Ehdr.e_shoff.getInt());
		for (int x = 0; x < elf32_Ehdr.e_shnum.getInt(); x++) {
			Elf32_Shdr elf32_Shdr = new Elf32_Shdr();
			elf32_Shdr.read(inputStream);
			elf32_Shdrs.add(elf32_Shdr);
		}

		
	}
	
	public void write(ELF32 elf, File file) throws Exception {
		FileOutputStream fo = new FileOutputStream(file);
		fo.close();
	}
		
	public static byte[] getByteArray_half(int value) {
		byte b[] = new byte[]{(byte) (value & 0xff), (byte) (value >> 8 & 0xff)};
//		for (int x = 0; x < 8; x++) {
//			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
//		}
		return b;
	}
	
	public static byte[] getByteArray_word(int value) {
		byte b[] = new byte[4];
		for (int x = 0; x < 4; x++) {
			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}
	
	@Override
	public String toString() {
		String s = "";
		s += elf32_Ehdr + "\n";
		for (Elf32_Shdr elf32_Shdr : elf32_Shdrs) {
			s += elf32_Shdr + "\n";
		}
		s += "------------------------------------\n";
		for (Elf32_Phdr elf32_Phdr : elf32_Phdrs) {
			s += elf32_Phdr + "\n";
		}
		return s;
	}

	public void secondWrite(RandomAccessFile fo, String anhor, byte[] bytes) throws IOException {
		long position = fo.getFilePointer();
		fo.seek(secondPassWrite.get(anhor));
		fo.write(bytes);
		fo.seek(position);
	}

	public void write(RandomAccessFile fo) throws FileNotFoundException, IOException {
		elf32_Ehdr = new Elf32_Ehdr();
 	         elf32_Ehdr.e_ident = new byte[]{0x7F, 0x45, 0x4C, 0x46, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
		elf32_Ehdr.e_type = new Elf32_Half(new byte[]{0x2, 0x0});
		elf32_Ehdr.e_machine =  new Elf32_Half(new byte[]{(byte)0xF3, 0x0});
		elf32_Ehdr.e_version = new Elf32_Word(new byte[]{0x1, 0x0, 0x0, 0x0});
		elf32_Ehdr.e_entry = new Elf32_Addr(ELF32.getByteArray_word(0x0));
		elf32_Ehdr.e_phoff = new Elf32_Off(ELF32.getByteArray_word(0x0));
		elf32_Ehdr.e_shoff = new Elf32_Off(ELF32.getByteArray_word(0x0));
		elf32_Ehdr.e_flags = new Elf32_Word(new byte[]{0x0, 0x0, 0x0, 0x0});
		elf32_Ehdr.e_ehsize = new Elf32_Half(new byte[]{(byte)Elf32_Ehdr.size(), 0x0});
		elf32_Ehdr.e_phentsize = new Elf32_Half(ELF32.getByteArray_half(Elf32_Phdr.size())); //Elf32_Phdr.size()
		elf32_Ehdr.e_phnum = new Elf32_Half(ELF32.getByteArray_half(elf32_Phdrs.size()));
		elf32_Ehdr.e_shentsize = new Elf32_Half(ELF32.getByteArray_half(Elf32_Shdr.size()));
		elf32_Ehdr.e_shnum = new Elf32_Half(ELF32.getByteArray_half(elf32_Shdrs.size()));
		elf32_Ehdr.e_shstrndx = new Elf32_Half(new byte[]{0x3, 0x0});
		elf32_Ehdr.write(fo, secondPassWrite);
		
			// program headers
		Elf32_Phdr elf32_Phdr = new Elf32_Phdr();
		secondWrite(fo, "e_phoff", ELF32.getByteArray_word((int) fo.getFilePointer()));

	
		elf32_Phdr.p_type = new Elf32_Word(ELF32.getByteArray_word(Elf32_Phdr.PT_LOAD));
		elf32_Phdr.p_vaddr = new Elf32_Addr(ELF32.getByteArray_word(0x004000b0));//physical address
		elf32_Phdr.p_offset = new Elf32_Off(ELF32.getByteArray_word(0x00012b));//virtual address
		elf32_Phdr.p_paddr = new Elf32_Addr(ELF32.getByteArray_word(0x004000b0));//file size
		elf32_Phdr.p_memsz = new Elf32_Word(ELF32.getByteArray_word(getWriteSection(".text").bytes.length));//flg
		elf32_Phdr.p_filesz = new Elf32_Word(ELF32.getByteArray_word(getWriteSection(".text").bytes.length));//memory size
		elf32_Phdr.p_align = new Elf32_Word(ELF32.getByteArray_word(4));
		elf32_Phdr.p_flags = new Elf32_Word(ELF32.getByteArray_word(Elf32_Phdr.PF_R | Elf32_Phdr.PF_X));//offset
		elf32_Phdr.write(fo);
	
		elf32_Phdr = new Elf32_Phdr();
		elf32_Phdr.p_type = new Elf32_Word(ELF32.getByteArray_word(Elf32_Phdr.PT_LOAD));
		elf32_Phdr.p_offset = new Elf32_Off(ELF32.getByteArray_word(0x00000000));
		elf32_Phdr.p_vaddr = new Elf32_Addr(ELF32.getByteArray_word(0x006000d8));
		elf32_Phdr.p_paddr = new Elf32_Addr(ELF32.getByteArray_word(0x006000d8));
		elf32_Phdr.p_memsz = new Elf32_Word(ELF32.getByteArray_word(getWriteSection(".data").bytes.length));
		elf32_Phdr.p_filesz = new Elf32_Word(ELF32.getByteArray_word(getWriteSection(".data").bytes.length));
		elf32_Phdr.p_align = new Elf32_Word(ELF32.getByteArray_word(1));
		elf32_Phdr.p_flags = new Elf32_Word(ELF32.getByteArray_word(Elf32_Phdr.PF_R | Elf32_Phdr.PF_W));
		elf32_Phdr.write(fo);
		
		secondWrite(fo, "e_phnum", ELF32.getByteArray_half(2));

		// sections
		String sectionsName[] = {".shstrtab", ".text", ".data"};

		secondWrite(fo, "e_shoff", ELF32.getByteArray_word((int) fo.getFilePointer()));

		Elf32_Shdr elf32_Shdr = new Elf32_Shdr();
		elf32_Shdr.sh_name = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_type = new Elf32_Word(ELF32.getByteArray_word(Elf32_Shdr.SHT_NULL));
		elf32_Shdr.sh_addr = new Elf32_Addr(ELF32.getByteArray_word(0000000000000000));
		elf32_Shdr.sh_offset = new Elf32_Off(ELF32.getByteArray_word(00000000));
		elf32_Shdr.sh_size = new Elf32_Word(ELF32.getByteArray_word(0000000000000000));
		elf32_Shdr.sh_entsize = new Elf32_Word(ELF32.getByteArray_word(0000000000000000));
		elf32_Shdr.sh_flags = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_link = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_info = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_addralign = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.write(fo, secondPassWrite, "");

		elf32_Shdr = new Elf32_Shdr();
		elf32_Shdr.sh_name = new Elf32_Word(ELF32.getByteArray_word(ELF32.getStringOffset(sectionsName, ".text")));
		elf32_Shdr.sh_type = new Elf32_Word(ELF32.getByteArray_word(Elf32_Shdr.SHT_PROGBITS));
		elf32_Shdr.sh_addr = new Elf32_Addr(ELF32.getByteArray_word(0x00000000004000b0));
		elf32_Shdr.sh_offset = new Elf32_Off(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_size = new Elf32_Word(ELF32.getByteArray_word(getWriteSection(".text").bytes.length));
		elf32_Shdr.sh_entsize = new Elf32_Word(ELF32.getByteArray_word(0000000000000000));
		elf32_Shdr.sh_flags = new Elf32_Word(ELF32.getByteArray_word(Elf32_Shdr.SHF_ALLOC | Elf32_Shdr.SHF_EXECINSTR));
		elf32_Shdr.sh_link = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_info = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_addralign = new Elf32_Word(ELF32.getByteArray_word(4));
		elf32_Shdr.write(fo, secondPassWrite, ".text");

		elf32_Shdr = new Elf32_Shdr();
		elf32_Shdr.sh_name = new Elf32_Word(ELF32.getByteArray_word(ELF32.getStringOffset(sectionsName, ".data")));
		elf32_Shdr.sh_type = new Elf32_Word(ELF32.getByteArray_word(Elf32_Shdr.SHT_PROGBITS));
		elf32_Shdr.sh_addr = new Elf32_Addr(ELF32.getByteArray_word(0x00000000006000d8));
		elf32_Shdr.sh_offset = new Elf32_Off(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_size = new Elf32_Word(ELF32.getByteArray_word(getWriteSection(".data").bytes.length));
		elf32_Shdr.sh_entsize = new Elf32_Word(ELF32.getByteArray_word(0000000000000000));
		elf32_Shdr.sh_flags = new Elf32_Word(ELF32.getByteArray_word(Elf32_Shdr.SHF_ALLOC | Elf32_Shdr.SHF_WRITE));
		elf32_Shdr.sh_link = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_info = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_addralign = new Elf32_Word(ELF32.getByteArray_word(1));
		elf32_Shdr.write(fo, secondPassWrite, ".data");

		elf32_Shdr = new Elf32_Shdr();
		elf32_Shdr.sh_name = new Elf32_Word(ELF32.getByteArray_word(ELF32.getStringOffset(sectionsName, ".shstrtab")));
		elf32_Shdr.sh_type = new Elf32_Word(ELF32.getByteArray_word(Elf32_Shdr.SHT_STRTAB));
		elf32_Shdr.sh_addr = new Elf32_Addr(ELF32.getByteArray_word(0000000000000000));
		elf32_Shdr.sh_offset = new Elf32_Off(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_size = new Elf32_Word(ELF32.getByteArray_word(getWriteSection(".shstrtab").bytes.length));
		elf32_Shdr.sh_entsize = new Elf32_Word(ELF32.getByteArray_word(0000000000000000));
		elf32_Shdr.sh_flags = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_link = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_info = new Elf32_Word(ELF32.getByteArray_word(0));
		elf32_Shdr.sh_addralign = new Elf32_Word(ELF32.getByteArray_word(1));
		elf32_Shdr.write(fo, secondPassWrite, ".shstrtab");

		secondWrite(fo, "e_shnum", ELF32.getByteArray_half(4));

		secondWrite(fo, ".shstrtab_offset", ELF32.getByteArray_word((int) fo.getFilePointer()));
		long begin = fo.getFilePointer();
		fo.write(0);
		for (String s : sectionsName) {
			fo.write(s.getBytes());
			fo.write(0);
		}
		long end = fo.getFilePointer();
		secondWrite(fo, ".shstrtab_size", ELF32.getByteArray_word((int) (end - begin)));

		// .text
		secondWrite(fo, ".text_offset", ELF32.getByteArray_word((int) fo.getFilePointer()));
		fo.write(getWriteSection(".text").bytes);
	}

	public ELF32WriteSection getWriteSection(String sectionName) {
		for (ELF32WriteSection section : writeSections) {
			if (section.name.equals(sectionName)) {
				return section;
			}
		}
		return null;
	}

	public static int getStringOffset(String[] str, String s) {
		int x = 1;
		for (String temp : str) {
			if (s.equals(temp)) {
				return x;
			}
			x += temp.length() + 1;
		}
		return 0;
	}
}