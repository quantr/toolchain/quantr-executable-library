/*
 * Copyright 2022 peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.qr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author peter
 */
public class QRLib {

	public static void write(QR qr, String jsonFileName) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
//		objectMapper.writeValue(new File(jsonFileName), qr);
        
		String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(qr);
		IOUtils.write(jsonString, new FileOutputStream(jsonFileName), "utf-8");
	}

	public static void read(String jsonFileName) throws FileNotFoundException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		String content = IOUtils.toString(new FileInputStream(new File(jsonFileName)), "UTF-8");
		QR qr = objectMapper.readValue(content, QR.class);

		System.out.println(String.format("Header{\n"
				+ "Magic Number: %s\n"
				+ "Endian: %s\n"
				+ "Target operating system ABI: %s\n"
				+ "Version of target operating system ABI: %s\n"
				+ "File type: %s\n"
				+ "Target instruction set architecture: %s\n"
				+ "Memory address of the entry point where the process starts executing: %s\n"
				+ "Points to start of the program header table: %d\n"
				+ "Points to start of the section header table: %d\n"
				+ "Size of this header: %d\n"
				+ "Size of the program header table entry: %d\n"
				+ "Numbers of entries in the program header table: %d\n"
				+ "Size of the section header table entry: %d\n"
				+ "Numbers of entries n the section header table: %d\n"
				+ "Index of the section header table entry that contains the section name: %d\n"
				+ "}\n",
				qr.header.magicNumber,
				qr.header.endian,
				qr.header.osABI,
				qr.header.osABIVer,
				qr.header.type,
				qr.header.isa,
				qr.header.entry,
				qr.header.phtStart,
				qr.header.shtStart,
				qr.header.hSize,
				qr.header.phteSize,
				qr.header.phteNum,
				qr.header.shteSize,
				qr.header.shteNum,
				qr.header.shtewsnIndex));

		for (ProgramSegment proSeg : qr.programSegments) {
			System.out.println(String.format("Program Segment{\n"
					+ "Type of program segment: %s\n"
					+ "Segment-dependent flags: %s\n"
					+ "Offset of the segment in the file image: %d\n"
					+ "Virtual address of the segment: %d\n"
					+ "Physical address of the segment: %d\n"
					+ "Size of bytes of the segment in the file image: %d\n"
					+ "Size in bytes of the segment in memory: %d\n"
					+ "Required alignment of the section: %d\n"
					+ "}\n",
					proSeg.type,
					proSeg.flag,
					proSeg.offset,
					proSeg.vAddress,
					proSeg.pAddress,
					proSeg.smiSize,
					proSeg.msSize,
					proSeg.alignment));
		}

		for (Section sec : qr.sections) {
			System.out.println(String.format("Section{\n"
					+ "Name of this section: %s\n"
					+ "Type of this header: %s\n"
					+ "Attributes of the section: %s\n"
					+ "Offset of the section in the file image: %d\n"
					+ "Size in bytes of the section in the file image: %d\n"
					+ "Section proSeg of an associated section: %d\n"
					+ "Extra information about the section: %s\n"
					+ "Required alignment of the section: %d\n"
					+ "Size of each entry for sections that contain fixed-size entries: %d\n"
					+ "}\n",
					sec.name,
					sec.type,
					sec.attribute,
					sec.vAddress,
					sec.size,
					sec.link,
					sec.info,
					sec.alignment,
					sec.entrySize));
		}
	}

	public static boolean validate(String jsonFileName) throws FileNotFoundException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		String content = IOUtils.toString(new FileInputStream(new File(jsonFileName)), "UTF-8");
		QR qr = objectMapper.readValue(content, QR.class);
		if (!qr.header.magicNumber.equals("QR")) {
			return false;
		}
		return true;
	}
}
