/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.qr;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ProgramSegment {
    public String type;		//	identifies the type of the segment e.g. loadable segment
	public String flag;		//	segment-dependent flags
	public int offset;		//	offset of the segment in the file image
	public long vAddress;	//	virtual address of the segment in memory
	public long pAddress;	//	physical address of the segment
	public int smiSize;		//	size of bytes of the segment in the file image
	public int msSize;		//	size in bytes of the segment in memory
	public int alignment;	//	contains the required alignment of the section

	public ProgramSegment(String type, String flag, int offset, long vAddress, long pAddress, int smiSize, int msSize, int alignment) {
		this.type = type;
		this.flag = flag;
		this.offset = offset;
		this.vAddress = vAddress;
		this.pAddress = pAddress;
		this.smiSize = smiSize;
		this.msSize = msSize;
		this.alignment = alignment;
	}
	
	public ProgramSegment(){
	}
}
