/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.qr;

/**
 *
 * @author Peter <peter@quantr.hk>
 */

public class Header {
	public String magicNumber;	//	constant signiture
	public String endian;		//	big-endian / little-endian
	public String osABI;		//	target operating system ABI
	public String osABIVer;		//	version of target operating system ABI
	public String type;			//	identifies object file type, e.g. executable file
	public String isa;			//  specifies target instruction set architecture
	public String entry;		//	memory address of the entry point where the process starts executing
	public int phtStart;		//	points to start of the program header table
	public int shtStart;			//	points to start of the section header table
	public int hSize;			//	size of this header
	public int phteSize;		//	size of the program header table entry
	public int phteNum;			//	the numbers of entries in the program header table
	public int shteSize;		//	size of the section header table entry
	public int shteNum;			//	the numbers of entries n the section header table
	public int shtewsnIndex;	//	contains the index of the section header table entry that contains the section name
//	
	public Header(String magicNumber, String endian, String osABI, String osABIVer, String type, String isa, String entry, int phtStart, int shtStart, int hSize, int phteSize, int phteNum, int shteSize, int shteNum, int shtewsnIndex){
		this.magicNumber = magicNumber;
		this.endian = endian;
		this.osABI = osABI;
		this.osABIVer = osABIVer;
		this.type = type;
		this.isa = isa;
		this.entry = entry;
		this.phtStart = phtStart;
		this.shtStart = shtStart;			
		this.hSize = hSize;			
		this.phteSize = phteSize;		
		this.phteNum = phteNum;			
		this.shteSize = shteSize;		
		this.shteNum = shteNum;			
		this.shtewsnIndex = shtewsnIndex;
	}
	
	public Header(){
	}
}
