/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.qr;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Section {

    public String name;		//	name of this section
	public String type;		//	type of this header, e.g. symbol table / string table
	public String attribute;//	attributes of the section e.g. writable / execuatble
	public long vAddress;	//	offset of the section in the file image
	public int size;		//	size in bytes of the section in the file image
	public int link;		//	contains the section index of an associated section
	public String info;		//	contains extra information about the section
	public int alignment;	//	contains the required alignment of the section
	public int entrySize;	//	contains the size, in bytes, of each entry, for sections that contain fixed-size entries.

	public Section(String name, String type, String attribute, long vAddress, int size, int link, String info, int alignment, int entrySize) {
		this.name = name;
		this.type = type;
		this.attribute = attribute;
		this.vAddress = vAddress;
		this.size = size;
		this.link = link;
		this.info = info;
		this.alignment = alignment;
		this.entrySize = entrySize;
	}
	
	public Section(){
	}
}
