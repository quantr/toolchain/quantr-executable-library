/*
 * Copyright 2019 Desmond Ng.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.pe.datatype;

import hk.quantr.executablelibrary.pe.datatype.PeBase;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Pe_Word extends PeBase {

	public int size = 4;
	public int alignment = 4;

	public Pe_Word() {
		bytes = new byte[size];
	}

	public Pe_Word(byte[] bytes) {
		this.bytes = bytes;
	}
}
