/*
 * Copyright 2019 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.pe;

import java.io.InputStream;
import java.util.ArrayList;

import hk.quantr.executablelibrary.elf64.ELF64WriteSection;
import static hk.quantr.executablelibrary.pe.PE.NumberOfLineNumbers;
import static hk.quantr.executablelibrary.pe.PE.NumberOfRelocations;
import static hk.quantr.executablelibrary.pe.PE.NumberOfSections;
import static hk.quantr.executablelibrary.pe.PE.PointerToLineNumbers;
import static hk.quantr.executablelibrary.pe.PE.PointerToRawData;
import static hk.quantr.executablelibrary.pe.PE.PointerToRelocations;
import static hk.quantr.executablelibrary.pe.PE.Section;
import static hk.quantr.executablelibrary.pe.PE.SectionCharacteristics;
import static hk.quantr.executablelibrary.pe.PE.SectionHeaderName;
import static hk.quantr.executablelibrary.pe.PE.SizeOfRawData;
import static hk.quantr.executablelibrary.pe.PE.VirtualAddress;
import static hk.quantr.executablelibrary.pe.PE.VirtualSize;
import static hk.quantr.executablelibrary.pe.PE.size;
import hk.quantr.executablelibrary.pe.datatype.Pe_Half;
import hk.quantr.executablelibrary.pe.datatype.Pe_Byte;
import hk.quantr.executablelibrary.pe.datatype.Pe_Word;
import hk.quantr.executablelibrary.pe.datatype.Pe_DWord;
import hk.quantr.executablelibrary.pe.datatype.Pe_Bit;
import hk.quantr.executablelibrary.pe.datatype.Pe_XByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class PE implements Serializable {

	//public DOS_MZ dos_mz;
	public PE pe;
	public String[] sectionsName;
	public HashMap<String, Integer> sectionOffset = new HashMap();
	public HashMap<String, Long> secondPassWrite = new HashMap();
	public ArrayList<ELF64WriteSection> writeSections = new ArrayList();

	public byte e_res[] = new byte[8];
	public byte e_res2[] = new byte[16];
	public byte dos_stub[] = new byte[64];
	public byte Architecture[] = new byte[8];

	public static int EI_MAG0 = 0;
	public static int EI_MAG1 = 1;
	public static int EI_MAG2 = 2;
	public static int EI_MAG3 = 3;
	public static int EI_CLASS = 4;
	public static int EI_DATA = 5;
	public static int EI_VERSION = 6;
	public static int EI_PAD = 7;

	public Pe_Half e_magic = new Pe_Half();
	public Pe_Half e_cblp = new Pe_Half();
	public Pe_Half e_cp = new Pe_Half();
	public Pe_Half e_crlc = new Pe_Half();
	public Pe_Half e_cparhdr = new Pe_Half();
	public Pe_Half e_minalloc = new Pe_Half();
	public Pe_Half e_maxalloc = new Pe_Half();
	public Pe_Half e_ss = new Pe_Half();
	public Pe_Half e_sp = new Pe_Half();
	public Pe_Half e_csum = new Pe_Half();
	public Pe_Half e_ip = new Pe_Half();
	public Pe_Half e_cs = new Pe_Half();
	public Pe_Half e_lfarlc = new Pe_Half();
	public Pe_Half e_ovno = new Pe_Half();
	public Pe_Half e_oemid = new Pe_Half();
	public Pe_Half e_oeminfo = new Pe_Half();
	public Pe_Word rubbish = new Pe_Word();
	public Pe_Word e_lfanew = new Pe_Word();
	public Pe_Word signature = new Pe_Word();
	public Pe_Half TargetMachine = new Pe_Half();
	public static Pe_Half NumberOfSections = new Pe_Half();
	public Pe_Word TimeDateStamp = new Pe_Word();
	public Pe_Word PointerToSymbolTable = new Pe_Word();
	public Pe_Word NumberOfSymbols = new Pe_Word();
	public Pe_Half SizeOfOptionalHeaders = new Pe_Half();
	public Pe_Half Characteristics = new Pe_Half();
	public Pe_Half Exe = new Pe_Half();
	public Pe_Bit InMajVer = new Pe_Bit();
	public Pe_Bit InMnrVer = new Pe_Bit();
	public Pe_Word SizeOfCode = new Pe_Word();
	public Pe_Word SizeOfInitializedData = new Pe_Word();
	public Pe_Word SizeOfUninitializedData = new Pe_Word();
	public Pe_Word AddressOfEntryPoint = new Pe_Word();
	public Pe_Word BaseOfCode = new Pe_Word();
	public Pe_Word BaseOfData = new Pe_Word();
	public Pe_Word ImageBase = new Pe_Word();
	public Pe_Word SectionAlignment = new Pe_Word();
	public Pe_Word FileAlignment = new Pe_Word();
	public Pe_Half MajorOSVersion = new Pe_Half();
	public Pe_Half MinorOSVersion = new Pe_Half();
	public Pe_Half MajorImageVersion = new Pe_Half();
	public Pe_Half MinorImageVersion = new Pe_Half();
	public Pe_Half MajorSubSystemVersion = new Pe_Half();
	public Pe_Half MinorSubSystemVersion = new Pe_Half();
	public Pe_Word Win32VersionValue = new Pe_Word();
	public Pe_Word SizeOfImage = new Pe_Word();
	public Pe_Word SizeOfHeaders = new Pe_Word();
	public Pe_Word Checksum = new Pe_Word();
	public Pe_Half Checksum2 = new Pe_Half();
	public Pe_Half DllCharacteristics = new Pe_Half();
	public Pe_Word SizeOfStackReserve = new Pe_Word();
	public Pe_Word SizeOfStackCommit = new Pe_Word();
	public Pe_Word SizeOfHeapReserve = new Pe_Word();
	public Pe_Word SizeOfHeapCommit = new Pe_Word();
	public Pe_Word LoaderFlags = new Pe_Word();
	public Pe_Word NumberOfRVAandSizes = new Pe_Word();
	public Pe_Word edataOffset = new Pe_Word();
	public Pe_Word edataSize = new Pe_Word();
	public Pe_Word idataOffset = new Pe_Word();
	public Pe_Word idataSize = new Pe_Word();
	public Pe_Word rsrcOffset = new Pe_Word();
	public Pe_Word rsrcSize = new Pe_Word();
	public Pe_Word pdataOffset = new Pe_Word();
	public Pe_Word pdataSize = new Pe_Word();
	public Pe_Word AttributeCertificateOffset = new Pe_Word();
	public Pe_Word AttributeCertificateSize = new Pe_Word();
	public Pe_Word relocOffset = new Pe_Word();
	public Pe_Word relocSize = new Pe_Word();
	public Pe_Word debugOffset = new Pe_Word();
	public Pe_Word debugSize = new Pe_Word();
	public Pe_Word GlobalPtrOffset = new Pe_Word();
	public Pe_Word Zeros = new Pe_Word();
	public Pe_Word tlsOffset = new Pe_Word();
	public Pe_Word tlsSize = new Pe_Word();
	public Pe_Word LoadConfigTableOffset = new Pe_Word();
	public Pe_Word LoadConfigTableSize = new Pe_Word();
	public Pe_Word BoundImportTableOffset = new Pe_Word();
	public Pe_Word BoundImportTableSize = new Pe_Word();
	public Pe_Word ImportAddressTableOffset = new Pe_Word();
	public Pe_Word ImportAddressTableSize = new Pe_Word();
	public Pe_Word DelayImportDescriptorOffset = new Pe_Word();
	public Pe_Word DelayImportDescriptorSize = new Pe_Word();
	public Pe_Word CLRRuntimeHeaderOffset = new Pe_Word();
	public Pe_Word CLRRuntimeHeaderSize = new Pe_Word();
	public Pe_Byte rubbish2 = new Pe_Byte();

	public static Pe_DWord[] SectionHeaderName;
	public static Pe_Word[] VirtualSize;
	public static Pe_Word[] VirtualAddress;
	public static Pe_Word[] SizeOfRawData;
	public static Pe_Word[] PointerToRawData;//starting point of a section
	public static Pe_Word[] PointerToRelocations;
	public static Pe_Word[] PointerToLineNumbers;
	public static Pe_Half[] NumberOfRelocations;
	public static Pe_Half[] NumberOfLineNumbers;
	public static Pe_Word[] SectionCharacteristics;
	public static Pe_XByte[] Section;

	public PE() {
	}

	public PE(InputStream inputStream) throws Exception {
		pe = new PE();
		read(inputStream);
	}

	void read(InputStream inputStream) throws Exception {
		byte bytes[] = inputStream.readAllBytes();

		inputStream = new ByteArrayInputStream(bytes);

		e_magic.read(inputStream);
		e_cblp.read(inputStream);
		e_cp.read(inputStream);
		e_crlc.read(inputStream);
		e_cparhdr.read(inputStream);
		e_minalloc.read(inputStream);
		e_maxalloc.read(inputStream);
		e_ss.read(inputStream);
		e_sp.read(inputStream);
		e_csum.read(inputStream);
		e_ip.read(inputStream);
		e_cs.read(inputStream);
		e_lfarlc.read(inputStream);
		e_ovno.read(inputStream);
		inputStream.read(e_res);
		e_oemid.read(inputStream);
		e_oeminfo.read(inputStream);
		inputStream.read(e_res2);
		rubbish.read(inputStream);
		e_lfanew.read(inputStream);
		inputStream.read(dos_stub);
		signature.read(inputStream);
		TargetMachine.read(inputStream);
		NumberOfSections.read(inputStream);
		TimeDateStamp.read(inputStream);
		PointerToSymbolTable.read(inputStream);
		NumberOfSymbols.read(inputStream);
		SizeOfOptionalHeaders.read(inputStream);
		Characteristics.read(inputStream);
		Exe.read(inputStream);
		InMajVer.read(inputStream);
		InMnrVer.read(inputStream);
		SizeOfCode.read(inputStream);
		SizeOfInitializedData.read(inputStream);
		SizeOfUninitializedData.read(inputStream);
		AddressOfEntryPoint.read(inputStream);
		BaseOfCode.read(inputStream);
		BaseOfData.read(inputStream);
		ImageBase.read(inputStream);
		SectionAlignment.read(inputStream);
		FileAlignment.read(inputStream);
		MajorOSVersion.read(inputStream);
		MinorOSVersion.read(inputStream);
		MajorImageVersion.read(inputStream);
		MinorImageVersion.read(inputStream);
		MajorSubSystemVersion.read(inputStream);
		MinorSubSystemVersion.read(inputStream);
		Win32VersionValue.read(inputStream);
		SizeOfImage.read(inputStream);
		SizeOfHeaders.read(inputStream);
		Checksum.read(inputStream);
		Checksum2.read(inputStream);
		DllCharacteristics.read(inputStream);
		SizeOfStackReserve.read(inputStream);
		SizeOfStackCommit.read(inputStream);
		SizeOfHeapReserve.read(inputStream);
		SizeOfHeapCommit.read(inputStream);
		LoaderFlags.read(inputStream);
		NumberOfRVAandSizes.read(inputStream);
		edataOffset.read(inputStream);
		edataSize.read(inputStream);
		idataOffset.read(inputStream);
		idataSize.read(inputStream);
		rsrcOffset.read(inputStream);
		rsrcSize.read(inputStream);
		pdataOffset.read(inputStream);
		pdataSize.read(inputStream);
		AttributeCertificateOffset.read(inputStream);
		AttributeCertificateSize.read(inputStream);
		relocOffset.read(inputStream);
		relocSize.read(inputStream);
		debugOffset.read(inputStream);
		debugSize.read(inputStream);
		inputStream.read(Architecture);
		GlobalPtrOffset.read(inputStream);
		Zeros.read(inputStream);
		tlsOffset.read(inputStream);
		tlsSize.read(inputStream);
		LoadConfigTableOffset.read(inputStream);
		LoadConfigTableSize.read(inputStream);
		BoundImportTableOffset.read(inputStream);
		BoundImportTableSize.read(inputStream);
		ImportAddressTableOffset.read(inputStream);
		ImportAddressTableSize.read(inputStream);
		DelayImportDescriptorOffset.read(inputStream);
		DelayImportDescriptorSize.read(inputStream);
		CLRRuntimeHeaderOffset.read(inputStream);
		CLRRuntimeHeaderSize.read(inputStream);
		Zeros.read(inputStream);
		Zeros.read(inputStream);
		SectionHeaderName = new Pe_DWord[(int) NumberOfSections.getInt()];
		VirtualSize = new Pe_Word[(int) NumberOfSections.getInt()];
		VirtualAddress = new Pe_Word[(int) NumberOfSections.getInt()];
		SizeOfRawData = new Pe_Word[(int) NumberOfSections.getInt()];
		PointerToRawData = new Pe_Word[(int) NumberOfSections.getInt()];
		PointerToRelocations = new Pe_Word[(int) NumberOfSections.getInt()];
		PointerToLineNumbers = new Pe_Word[(int) NumberOfSections.getInt()];
		NumberOfRelocations = new Pe_Half[(int) NumberOfSections.getInt()];
		NumberOfLineNumbers = new Pe_Half[(int) NumberOfSections.getInt()];
		SectionCharacteristics = new Pe_Word[(int) NumberOfSections.getInt()];

		for (int i = 0; i < NumberOfSections.getInt(); i++) {
			SectionHeaderName[i] = new Pe_DWord();
			SectionHeaderName[i].read(inputStream);
			VirtualSize[i] = new Pe_Word();
			VirtualSize[i].read(inputStream);
			VirtualAddress[i] = new Pe_Word();
			VirtualAddress[i].read(inputStream);
			SizeOfRawData[i] = new Pe_Word();
			SizeOfRawData[i].read(inputStream);
			PointerToRawData[i] = new Pe_Word();
			PointerToRawData[i].read(inputStream);
			PointerToRelocations[i] = new Pe_Word();
			PointerToRelocations[i].read(inputStream);
			PointerToLineNumbers[i] = new Pe_Word();
			PointerToLineNumbers[i].read(inputStream);
			NumberOfRelocations[i] = new Pe_Half();
			NumberOfRelocations[i].read(inputStream);
			NumberOfLineNumbers[i] = new Pe_Half();
			NumberOfLineNumbers[i].read(inputStream);
			SectionCharacteristics[i] = new Pe_Word();
			SectionCharacteristics[i].read(inputStream);
		}
		int size = 0;
		int currentAddress = 0;
		int startAddress = 0;
		currentAddress = size();
		Section = new Pe_XByte[(int) NumberOfSections.getInt()];

		for (int a = 0; a < NumberOfSections.getInt(); a++) {
			size = (int) toInt(SizeOfRawData[a]);
			startAddress = (int) toInt(PointerToRawData[a]);

			while (startAddress > currentAddress) {
				rubbish2.read(inputStream);
				currentAddress++;
			}
			Section[a] = new Pe_XByte(size);
			Section[a].read(inputStream);
			currentAddress += size;
		}

	}

	public long toInt(Pe_Word value) {

		long result = 0;
		result = value.getInt();
		return result;
	}

	public long toInt(Pe_Half value) {
		long result = 0;
		result = value.getInt();
		return result;
	}

	public String toString() {
		String s = "";
		s += "PE Header:\n";
		s += String.format("%-30s %s %n", "Signature", e_magic);
		s += String.format("%-30s %s %n", "Bytes on Last Page of File", e_cblp);
		s += String.format("%-30s %s %n", "PagesInFile", e_cp);
		s += String.format("%-30s %s %n", "Relocations", e_crlc);
		s += String.format("%-30s %s %n", "HeaderSizeInParagraph", e_cparhdr);
		s += String.format("%-30s %s %n", "MinExtraParagraphNeeded", e_minalloc);
		s += String.format("%-30s %s %n", "MaxExtraParagraphNeeded", e_maxalloc);
		s += String.format("%-30s %s %n", "Initial (relative) SS", e_ss);
		s += String.format("%-30s %s %n", "Initial (relative) SP", e_sp);
		s += String.format("%-30s %s %n", "Checksum", e_csum);
		s += String.format("%-30s %s %n", "Initial IP", e_ip);
		s += String.format("%-30s %s %n", "Initial (relative) CS", e_cs);
		s += String.format("%-30s %s %n", "FileAddOfRelocTable", e_lfarlc);
		s += String.format("%-30s %s %n", "OverlayNumber", e_ovno);
		s += String.format("%-30s ", "Reserved");
		for (int x = 0; x < 8; x++) {
			s += String.format("%02X", e_res[x]) + " ";
		}
		s += "\n";
		s += String.format("%-30s %s %n", "OEMIdentifier", e_oemid);
		s += String.format("%-30s %s %n", "OEMInformation", e_oeminfo);
		s += String.format("%-30s ", "Reserved2");
		for (int x = 0; x < 16; x++) {
			s += String.format("%02X", e_res2[x]) + " ";
		}
		s += "\n";
		//s += String.format("%-30s %s %n", "rubbish", rubbish);
		s += String.format("%-30s %s %n", "offset to PE signature", e_lfanew);
		s += String.format("%-30s ", "dos-stub");
		for (int x = 0; x < 64; x++) {
			s += String.format("%02X", dos_stub[x]) + " ";
		}
		s += "\n";
		s += String.format("%-30s %s %n", "PE signature", signature);
		s += String.format("%-30s %s %n", "Target Machine", TargetMachine);
		s += String.format("%-30s %s %n", "Number Of Sections", NumberOfSections);
		s += String.format("%-30s %s %n", "Time Date Stamp", TimeDateStamp);
		s += String.format("%-30s %s %n", "Pointer To Symbol Table", PointerToSymbolTable);
		s += String.format("%-30s %s %n", "Number of symbols", NumberOfSymbols);
		s += String.format("%-30s %s %n", "Size of optional headers", SizeOfOptionalHeaders);
		s += String.format("%-30s %s %n", "Characteristics", Characteristics);
		s += String.format("%-30s %s %n", "Exe", Exe);
		s += String.format("%-30s %s %n", "InMajVer", InMajVer);
		s += String.format("%-30s %s %n", "InMnrVer", InMnrVer);
		s += String.format("%-30s %s %n", "Size of code", SizeOfCode);
		s += String.format("%-30s %s %n", "Size of initialized data", SizeOfInitializedData);
		s += String.format("%-30s %s %n", "Size of uninitialized data", SizeOfUninitializedData);
		s += String.format("%-30s %s %n", "Address of entry point", AddressOfEntryPoint);
		s += String.format("%-30s %s %n", "Base of code", BaseOfCode);
		s += String.format("%-30s %s %n", "Base of data", BaseOfData);
		s += String.format("%-30s %s %n", "Image base", ImageBase);
		s += String.format("%-30s %s %n", "Section alignment", SectionAlignment);
		s += String.format("%-30s %s %n", "File alignment", FileAlignment);
		s += String.format("%-30s %s %n", "Major OS version", MajorOSVersion);
		s += String.format("%-30s %s %n", "Minor OS version", MinorOSVersion);
		s += String.format("%-30s %s %n", "Major image version", MajorImageVersion);
		s += String.format("%-30s %s %n", "Minor image version", MinorImageVersion);
		s += String.format("%-30s %s %n", "Major subsystem version", MajorSubSystemVersion);
		s += String.format("%-30s %s %n", "Minor subsystem version", MinorSubSystemVersion);
		s += String.format("%-30s %s %n", "Win32 version value", Win32VersionValue);
		s += String.format("%-30s %s %n", "Size of image", SizeOfImage);
		s += String.format("%-30s %s %n", "Size of headers", SizeOfHeaders);
		s += String.format("%-30s %s %n", "Checksum", Checksum);
		s += String.format("%-30s %s %n", "Checksum", Checksum2);
		s += String.format("%-30s %s %n", "Dll characteristics", DllCharacteristics);
		s += String.format("%-30s %s %n", "Size of stack reserve", SizeOfStackReserve);
		s += String.format("%-30s %s %n", "Size of stack commit", SizeOfStackCommit);
		s += String.format("%-30s %s %n", "Size of heap reserve", SizeOfHeapReserve);
		s += String.format("%-30s %s %n", "Size of heap commit", SizeOfHeapCommit);
		s += String.format("%-30s %s %n", "Loader flags", LoaderFlags);
		s += String.format("%-30s %s %n", "Number of RVA and sizes", NumberOfRVAandSizes);
		s += String.format("%-30s %s %n", "Export table offset", edataOffset);
		s += String.format("%-30s %s %n", "Export table size", edataSize);
		s += String.format("%-30s %s %n", "Import table offset", idataOffset);
		s += String.format("%-30s %s %n", "Import table size", idataSize);
		s += String.format("%-30s %s %n", "Resource table offset", rsrcOffset);
		s += String.format("%-30s %s %n", "Resource table size", rsrcSize);
		s += String.format("%-30s %s %n", "Exception table offset", pdataOffset);
		s += String.format("%-30s %s %n", "Exception table size", pdataSize);
		s += String.format("%-30s %s %n", "attribute certificate offset (image)", AttributeCertificateOffset);
		s += String.format("%-30s %s %n", "attribute certificate size (image)", AttributeCertificateSize);
		s += String.format("%-30s %s %n", "Base relocation table offset", relocOffset);
		s += String.format("%-30s %s %n", "Base relocation table size", relocSize);
		s += String.format("%-30s %s %n", ".debug offset", debugOffset);
		s += String.format("%-30s %s %n", ".debug size", debugSize);
		s += String.format("%-30s ", "architecture (reserved -0x0)");
		for (int x = 0; x < 8; x++) {
			s += String.format("%02X", Architecture[x]) + " ";
		}
		s += "\n";
		s += String.format("%-30s %s %n", "global ptr offset", GlobalPtrOffset);
		s += String.format("%-30s %s %n", ".tls offset", tlsOffset);
		s += String.format("%-30s %s %n", ".tls size", tlsSize);
		s += String.format("%-30s %s %n", "Load config table offset", LoadConfigTableOffset);
		s += String.format("%-30s %s %n", "Load config table size", LoadConfigTableSize);
		s += String.format("%-30s %s %n", "Bound import table offset", BoundImportTableOffset);
		s += String.format("%-30s %s %n", "Bound import table size", BoundImportTableSize);
		s += String.format("%-30s %s %n", "Import address table offset", ImportAddressTableOffset);
		s += String.format("%-30s %s %n", "Import address table size", ImportAddressTableSize);
		s += String.format("%-30s %s %n", "Delay import decriptor offset", DelayImportDescriptorOffset);
		s += String.format("%-30s %s %n", "Delay import decriptor size", DelayImportDescriptorSize);
		s += String.format("%-30s %s %n", "CLR runtime header offset", CLRRuntimeHeaderOffset);
		s += String.format("%-30s %s %n", "CLR runtime header size", CLRRuntimeHeaderSize);
		// s += String.format("%-30s %s %n", "CLR runtime header size", SectionHeaderName);
		for (int x = 0; x < NumberOfSections.getInt(); x++) {
			s += String.format("%-30s ", "Section header name");
			s += String.format("%s", SectionHeaderName[x]) + " ";
			s += "\n";
			s += String.format("%-30s ", "Virtual size");
			s += String.format("%s", VirtualSize[x]) + " ";
			s += "\n";
			s += String.format("%-30s ", "Virtual address");
			s += String.format("%s", VirtualAddress[x]) + " ";
			s += "\n";
			s += String.format("%-30s ", "Size of raw data");
			s += String.format("%s", SizeOfRawData[x]) + " ";
			s += "\n";
			s += String.format("%-30s ", "Pointer to raw data");
			s += String.format("%s", PointerToRawData[x]) + " ";
			s += "\n";
			s += String.format("%-30s ", "Pointer to relocations");
			s += String.format("%s", PointerToRelocations[x]) + " ";
			s += "\n";
			s += String.format("%-30s ", "Pointer to line numbers");
			s += String.format("%s", PointerToLineNumbers[x]) + " ";
			s += "\n";
			s += String.format("%-30s ", "Number of relocations");
			s += String.format("%s", NumberOfRelocations[x]) + " ";
			s += "\n";
			s += String.format("%-30s ", "Number of line numbers");
			s += String.format("%s", NumberOfLineNumbers[x]) + " ";
			s += "\n";
			s += String.format("%-30s ", "Chracteristics");
			s += String.format("%s", SectionCharacteristics[x]) + " ";
			s += "\n";
		}

		for (int x = 0; x < Section.length; x++) {
			s += String.format("%-20s ", "section " + x);
			s += String.format("%s", Section[x]) + " ";
			s += "\n";
		}
		return s;
	}

	public void write(RandomAccessFile fo) throws IOException {
		fo.write(e_magic.bytes);
		fo.write(e_cblp.bytes);
		fo.write(e_cp.bytes);
		fo.write(e_crlc.bytes);
		fo.write(e_cparhdr.bytes);
		fo.write(e_minalloc.bytes);
		fo.write(e_maxalloc.bytes);
		fo.write(e_ss.bytes);
		fo.write(e_sp.bytes);
		fo.write(e_csum.bytes);
		fo.write(e_ip.bytes);
		fo.write(e_cs.bytes);
		fo.write(e_lfarlc.bytes);
		fo.write(e_ovno.bytes);
		fo.write(e_res);
		fo.write(e_oemid.bytes);
		fo.write(e_oeminfo.bytes);
		fo.write(e_res2);
		fo.write(rubbish.bytes);
		fo.write(e_lfanew.bytes);
		fo.write(rubbish2.bytes);
		fo.write(dos_stub);
		fo.write(signature.bytes);
		fo.write(TargetMachine.bytes);
		fo.write(NumberOfSections.bytes);
		fo.write(TimeDateStamp.bytes);
		fo.write(PointerToSymbolTable.bytes);
		fo.write(NumberOfSymbols.bytes);
		fo.write(SizeOfOptionalHeaders.bytes);
		fo.write(Characteristics.bytes);
		fo.write(Exe.bytes);
		fo.write(InMajVer.bytes);
		fo.write(InMnrVer.bytes);
		fo.write(SizeOfCode.bytes);
		fo.write(SizeOfInitializedData.bytes);
		fo.write(SizeOfUninitializedData.bytes);
		fo.write(AddressOfEntryPoint.bytes);
		fo.write(BaseOfCode.bytes);
		fo.write(BaseOfData.bytes);
		fo.write(ImageBase.bytes);
		fo.write(SectionAlignment.bytes);
		fo.write(FileAlignment.bytes);
		fo.write(MajorOSVersion.bytes);
		fo.write(MinorOSVersion.bytes);
		fo.write(MajorImageVersion.bytes);
		fo.write(MinorImageVersion.bytes);
		fo.write(MajorSubSystemVersion.bytes);
		fo.write(MinorSubSystemVersion.bytes);
		fo.write(Win32VersionValue.bytes);
		fo.write(SizeOfImage.bytes);
		fo.write(SizeOfHeaders.bytes);
		fo.write(Checksum.bytes);
		fo.write(Checksum2.bytes);
		fo.write(DllCharacteristics.bytes);
		fo.write(SizeOfStackReserve.bytes);
		fo.write(SizeOfStackCommit.bytes);
		fo.write(SizeOfHeapReserve.bytes);
		fo.write(SizeOfHeapCommit.bytes);
		fo.write(LoaderFlags.bytes);
		fo.write(NumberOfRVAandSizes.bytes);
		fo.write(edataOffset.bytes);
		fo.write(edataSize.bytes);
		fo.write(idataOffset.bytes);
		fo.write(idataSize.bytes);
		fo.write(rsrcOffset.bytes);
		fo.write(rsrcSize.bytes);
		fo.write(pdataOffset.bytes);
		fo.write(pdataSize.bytes);
		fo.write(AttributeCertificateOffset.bytes);
		fo.write(AttributeCertificateSize.bytes);
		fo.write(relocOffset.bytes);
		fo.write(relocSize.bytes);
		fo.write(debugOffset.bytes);
		fo.write(debugSize.bytes);
		fo.write(Architecture);
		fo.write(GlobalPtrOffset.bytes);
		fo.write(Zeros.bytes);
		fo.write(tlsOffset.bytes);
		fo.write(tlsSize.bytes);
		fo.write(LoadConfigTableOffset.bytes);
		fo.write(LoadConfigTableSize.bytes);
		fo.write(BoundImportTableOffset.bytes);
		fo.write(BoundImportTableSize.bytes);
		fo.write(ImportAddressTableOffset.bytes);
		fo.write(ImportAddressTableSize.bytes);
		fo.write(DelayImportDescriptorOffset.bytes);
		fo.write(DelayImportDescriptorSize.bytes);
		fo.write(CLRRuntimeHeaderOffset.bytes);
		fo.write(CLRRuntimeHeaderSize.bytes);
		fo.write(Zeros.bytes);
		fo.write(Zeros.bytes);
		//secondPassWrite.put("e_phoff", fo.getFilePointer());

	}

	/*	public void write(PE dos, File file) throws Exception {
		FileOutputStream fo = new FileOutputStream(file);
		fo.close();
	}
        void write(RandomAccessFile fo, HashMap<String, Long> secondPassWrite) throws IOException {
		fo.write(e_magic.bytes);
		fo.write(e_cblp.bytes);
		fo.write(e_cp.bytes);
		fo.write(e_crlc.bytes);
		fo.write(e_cparhdr.bytes);
		fo.write(e_minalloc.bytes);
		fo.write(e_maxalloc.bytes);
		fo.write(e_ss.bytes);
		fo.write(e_sp.bytes);
		fo.write(e_csum.bytes);
		fo.write(e_ip.bytes);
		fo.write(e_cs.bytes);
		fo.write(e_lfarlc.bytes);
		fo.write(e_ovno.bytes);
		fo.write(e_res);
		fo.write(e_oemid.bytes);
		fo.write(e_oeminfo.bytes);
		fo.write(e_res2);
		fo.write(rubbish.bytes);
		fo.write(e_lfanew.bytes);
	}*/

	public int sectionHeaderSize() {
		return SectionHeaderName[0].size
				+ VirtualSize[0].size
				+ VirtualAddress[0].size
				+ SizeOfRawData[0].size
				+ PointerToRawData[0].size
				+ PointerToRelocations[0].size
				+ PointerToLineNumbers[0].size
				+ NumberOfRelocations[0].size
				+ NumberOfLineNumbers[0].size
				+ SectionCharacteristics[0].size;
	}

	public static int size() {
		PE temp = new PE();

		int b = 0;
		int sectionTotal = (int) NumberOfSections.getInt();

		for (int a = 0; a < sectionTotal; a++) {
			b += SectionHeaderName[0].size
					+ VirtualSize[0].size
					+ VirtualAddress[0].size
					+ SizeOfRawData[0].size
					+ PointerToRawData[0].size
					+ PointerToRelocations[0].size
					+ PointerToLineNumbers[0].size
					+ NumberOfRelocations[0].size
					+ NumberOfLineNumbers[0].size
					+ SectionCharacteristics[0].size;

		}

		return temp.e_magic.size
				+ temp.e_cblp.size
				+ temp.e_cp.size
				+ temp.e_crlc.size
				+ temp.e_cparhdr.size
				+ temp.e_minalloc.size
				+ temp.e_maxalloc.size
				+ temp.e_ss.size
				+ temp.e_sp.size
				+ temp.e_csum.size
				+ temp.e_ip.size
				+ temp.e_cs.size
				+ temp.e_lfarlc.size
				+ temp.e_ovno.size
				+ temp.e_res.length
				+ temp.e_oemid.size
				+ temp.e_oeminfo.size
				+ temp.e_res2.length
				+ temp.rubbish.size
				+ temp.e_lfanew.size
				+ temp.dos_stub.length
				+ temp.signature.size
				+ temp.TargetMachine.size
				+ temp.NumberOfSections.size
				+ temp.TimeDateStamp.size
				+ temp.PointerToSymbolTable.size
				+ temp.NumberOfSymbols.size
				+ temp.SizeOfOptionalHeaders.size
				+ temp.Characteristics.size
				+ temp.Exe.size
				+ temp.InMajVer.size
				+ temp.InMnrVer.size
				+ temp.SizeOfCode.size
				+ temp.SizeOfInitializedData.size
				+ temp.SizeOfUninitializedData.size
				+ temp.AddressOfEntryPoint.size
				+ temp.BaseOfCode.size
				+ temp.BaseOfData.size
				+ temp.ImageBase.size
				+ temp.SectionAlignment.size
				+ temp.FileAlignment.size
				+ temp.MajorOSVersion.size
				+ temp.MinorOSVersion.size
				+ temp.MajorImageVersion.size
				+ temp.MinorImageVersion.size
				+ temp.MajorSubSystemVersion.size
				+ temp.MinorSubSystemVersion.size
				+ temp.Win32VersionValue.size
				+ temp.SizeOfImage.size
				+ temp.SizeOfHeaders.size
				+ temp.Checksum.size
				+ temp.Checksum2.size
				+ temp.DllCharacteristics.size
				+ temp.SizeOfStackReserve.size
				+ temp.SizeOfStackCommit.size
				+ temp.SizeOfHeapReserve.size
				+ temp.SizeOfHeapCommit.size
				+ temp.LoaderFlags.size
				+ temp.NumberOfRVAandSizes.size
				+ temp.edataOffset.size
				+ temp.edataSize.size
				+ temp.idataOffset.size
				+ temp.idataSize.size
				+ temp.rsrcOffset.size
				+ temp.rsrcSize.size
				+ temp.pdataOffset.size
				+ temp.pdataSize.size
				+ temp.AttributeCertificateOffset.size
				+ temp.AttributeCertificateSize.size
				+ temp.relocOffset.size
				+ temp.relocSize.size
				+ temp.debugOffset.size
				+ temp.debugSize.size
				+ temp.Architecture.length
				+ temp.GlobalPtrOffset.size
				+ temp.Zeros.size
				+ temp.tlsOffset.size
				+ temp.tlsSize.size
				+ temp.LoadConfigTableOffset.size
				+ temp.LoadConfigTableSize.size
				+ temp.BoundImportTableOffset.size
				+ temp.BoundImportTableSize.size
				+ temp.ImportAddressTableOffset.size
				+ temp.ImportAddressTableSize.size
				+ temp.DelayImportDescriptorOffset.size
				+ temp.DelayImportDescriptorSize.size
				+ temp.CLRRuntimeHeaderOffset.size
				+ temp.CLRRuntimeHeaderSize.size
				+ temp.Zeros.size
				+ temp.Zeros.size
				+ b;

	}

	public static byte[] getByteArray_half(int value) {
		byte b[] = new byte[]{(byte) (value & 0xff), (byte) (value >> 8 & 0xff)};
//		for (int x = 0; x < 8; x++) {
//			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
//		}
		return b;
	}

	public static byte[] getByteArray_word(int value) {
		byte b[] = new byte[4];
		for (int x = 0; x < 4; x++) {
			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}

	public static byte[] getByteArray_xword(int val) {
		long value = val;
		byte b[] = new byte[8];
		for (int x = 0; x < 8; x++) {
//			System.out.println(">> " + (8 - x - 1) + " > " + ((byte) ((value >> (x * 8)) & 0xffL)));
			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}

	public static byte[] getByteArray_xword(long value) {
		byte b[] = new byte[8];
		for (int x = 0; x < 8; x++) {
//			System.out.println(">> " + (8 - x - 1) + " > " + ((byte) ((value >> (x * 8)) & 0xffL)));
			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}

	public void secondWrite(RandomAccessFile fo, String anhor, byte[] bytes) throws IOException {
		long position = fo.getFilePointer();
		fo.seek(secondPassWrite.get(anhor));
		fo.write(bytes);
		fo.seek(position);
	}

	/*public void write(RandomAccessFile fo) throws FileNotFoundException, IOException {
		pe = new PE();
		//dos_mz.e_magic = new byte[]{0x5A, 0x4D, 0x4C, 0x46, 0x02, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
		pe.e_magic = new Pe_Half(new byte[]{0x2, 0x0});
		pe.e_cblp = new Pe_Half(new byte[]{0x2, 0x0});
		pe.e_cp = new Pe_Half(new byte[]{0x3E, 0x0});
		pe.e_crlc = new Pe_Half(new byte[]{0x1, 0x0, 0x0, 0x0});
		pe.e_cparhdr = new Pe_Half(new byte[]{0x1, 0x0, 0x0, 0x0});
		pe.e_minalloc = new Pe_Half(PE.getByteArray_xword(0x4000b0));
		pe.e_maxalloc = new Pe_Half(PE.getByteArray_xword(0));
		pe.e_ss = new Pe_Half(PE.getByteArray_xword(0x0));
		pe.e_sp = new Pe_Half(new byte[]{0x0, 0x0, 0x0, 0x0});
		pe.e_csum = new Pe_Half(new byte[]{0x40, 0x0});
		pe.e_ip = new Pe_Half(new byte[]{0x38, 0x0});
		//	dos_mz.e_phnum = new Elf64_Half(Header.getByteArray_half(elf64_Phdrs.size()));
		//	dos_mz.e_shentsize = new Elf64_Half(new byte[]{0x40, 0x0});
		//	dos_mz.e_shnum = new Elf64_Half(Header.getByteArray_half(elf64_Shdrs.size()));
		//	dos_mz.e_shstrndx = new Elf64_Half(new byte[]{0x3, 0x0});
		//	dos_mz.write(fo, secondPassWrite);

		/*	// sections
		String sectionsName[] = {".shstrtab", ".text", ".data"};
		System.out.println(">>>" + fo.getFilePointer());

		secondWrite(fo, "e_shoff", Header.getByteArray_xword(fo.getFilePointer()));

		secondWrite(fo, "e_shnum", Header.getByteArray_half(4));

		secondWrite(fo, ".shstrtab_offset", Header.getByteArray_xword(fo.getFilePointer()));
		long begin = fo.getFilePointer();
		fo.write(0);
		for (String s : sectionsName) {
			fo.write(s.getBytes());
			fo.write(0);
		}
		long end = fo.getFilePointer();
		secondWrite(fo, ".shstrtab_size", Header.getByteArray_xword(end - begin));

		// .text
		secondWrite(fo, ".text_offset", Header.getByteArray_xword(fo.getFilePointer()));
		fo.write(getWriteSection(".text").bytes);*/
	//}
	public ELF64WriteSection getWriteSection(String sectionName) {
		for (ELF64WriteSection section : writeSections) {
			if (section.name.equals(sectionName)) {
				return section;
			}
		}
		return null;
	}

	public static int getStringOffset(String[] str, String s) {
		int x = 1;
		for (String temp : str) {
			if (s.equals(temp)) {
				return x;
			}
			x += temp.length() + 1;
		}
		return 0;
	}

}
