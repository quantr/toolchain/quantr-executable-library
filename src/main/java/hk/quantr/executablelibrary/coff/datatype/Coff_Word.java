/*
 * Copyright 2019 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.coff.datatype;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Coff_Word extends CoffBase {

	public int size = 2;
	public int alignment = 2;

	public Coff_Word() {
		bytes = new byte[size];
	}

	public Coff_Word(byte[] bytes) {
		this.bytes = bytes;
	}
}
