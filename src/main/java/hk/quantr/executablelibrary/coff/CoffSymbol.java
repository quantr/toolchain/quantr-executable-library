package hk.quantr.executablelibrary.coff;

import com.google.gson.internal.LinkedTreeMap;
import hk.quantr.javalib.CommonLib;
import hk.quantr.executablelibrary.coff.datatype.Coff_Byte;
import hk.quantr.executablelibrary.coff.datatype.Coff_Dword;
import hk.quantr.executablelibrary.coff.datatype.Coff_Qword;
import hk.quantr.executablelibrary.coff.datatype.Coff_Word;
import java.io.InputStream;
import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CoffSymbol {

	public Coff_Qword name = new Coff_Qword();
	public Coff_Dword value = new Coff_Dword();
	public Coff_Word sectionNumber = new Coff_Word();
	public Coff_Word type = new Coff_Word();
	public Coff_Byte storageClass = new Coff_Byte();
	public Coff_Byte numberOfAuxSymbols = new Coff_Byte();
	public LinkedTreeMap<String, Object> auxiliarySymbolRecords = new LinkedTreeMap<>();
	String stringTable;
	ArrayList<String> strings;

	CoffSymbol(String stringTable, ArrayList<String> strings) {
		this.stringTable = stringTable;
	}

	void read(InputStream inputStream) throws Exception {
		name.read(inputStream);
		value.read(inputStream);
		sectionNumber.read(inputStream);
		type.read(inputStream);
		storageClass.read(inputStream);
		numberOfAuxSymbols.read(inputStream);
	}

	public String toString() {
		String s = "";
		if (name.bytes[0] == 0 && name.bytes[1] == 0 && name.bytes[2] == 0 && name.bytes[3] == 0) {
			int offset = (int) CommonLib.getInt(name.bytes, 4) - 4;
			String name = stringTable.substring(offset, stringTable.indexOf(Character.toString((char) 0), offset));
			s += String.format("%-30s %s %n", "Long name", name);
		} else {
			s += String.format("%-30s %s %n", "Short name", name.getString());
		}
		s += String.format("%-30s %s %n", "Value", value);
		s += String.format("%-30s %s %n", "SectionNumber", sectionNumber);
		s += String.format("%-30s %s %n", "Type", type);
		s += String.format("%-30s %s %n", "StorageClass", storageClass);
		s += String.format("%-30s %s %n", "NumberOfAuxSymbols", numberOfAuxSymbols);
		if (auxiliarySymbolRecords.size() > 0) {
			s += "Auxiliary Symbol Records\n";
			for (String key : auxiliarySymbolRecords.keySet()) {
				s += " + " + String.format("%-30s %s %n", key, auxiliarySymbolRecords.get(key));;
			}
		}
		return s;
	}
}
