package hk.quantr.executablelibrary.coff;

import hk.quantr.javalib.CommonLib;
import hk.quantr.executablelibrary.coff.datatype.Coff_Dword;
import hk.quantr.executablelibrary.coff.datatype.Coff_Qword;
import hk.quantr.executablelibrary.coff.datatype.Coff_Word;
import java.io.InputStream;
import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CoffSection {

	public Coff_Qword name = new Coff_Qword();
	public Coff_Dword virtualSize = new Coff_Dword();
	public Coff_Dword virtualAddress = new Coff_Dword();
	public Coff_Dword sizeOfRawData = new Coff_Dword();
	public Coff_Dword pointerToRawData = new Coff_Dword();
	public Coff_Dword pointerToRelocations = new Coff_Dword();
	public Coff_Dword pointerToLinenumbers = new Coff_Dword();
	public Coff_Word numberOfRelocations = new Coff_Word();
	public Coff_Word numberOfLinenumbers = new Coff_Word();
	public Coff_Dword characteristics = new Coff_Dword();

	public byte bytes[];

	public ArrayList<CoffRelocation> relocations = new ArrayList<CoffRelocation>();

	void read(InputStream inputStream) throws Exception {
		name.read(inputStream);
		virtualSize.read(inputStream);
		virtualAddress.read(inputStream);
		sizeOfRawData.read(inputStream);
		pointerToRawData.read(inputStream);
		pointerToRelocations.read(inputStream);
		pointerToLinenumbers.read(inputStream);
		numberOfRelocations.read(inputStream);
		numberOfLinenumbers.read(inputStream);
		characteristics.read(inputStream);
	}

	public String toString() {
		String s = "";

		s += String.format("%-30s %s %n", "Name", name.getString());
		s += String.format("%-30s %s %n", "VirtualSize", virtualSize);
		s += String.format("%-30s %s %n", "VirtualAddress", virtualAddress);
		s += String.format("%-30s %s %n", "SizeOfRawData", sizeOfRawData);
		s += String.format("%-30s %s %n", "PointerToRawData", pointerToRawData);
		s += String.format("%-30s %s %n", "PointerToRelocations", pointerToRelocations);
		s += String.format("%-30s %s %n", "PointerToLinenumbers", pointerToLinenumbers);
		s += String.format("%-30s %s %n", "NumberOfRelocations", numberOfRelocations);
		s += String.format("%-30s %s %n", "NumberOfLinenumbers", numberOfLinenumbers);
		s += String.format("%-30s %s %n", "Characteristics", characteristics);
		s += "\n";
		s += name.getString() + " : " + CommonLib.getHexString(bytes, " ", false) + "\n";
		s += "\n";
		s += "Relocations\n";
		for (CoffRelocation relocation : relocations) {
			s += relocation;
			s += "-----------------------------------------\n";
		}

		return s;
	}
}
