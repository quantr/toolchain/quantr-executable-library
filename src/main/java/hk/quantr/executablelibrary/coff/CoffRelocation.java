package hk.quantr.executablelibrary.coff;

import hk.quantr.executablelibrary.coff.datatype.Coff_Dword;
import hk.quantr.executablelibrary.coff.datatype.Coff_Qword;
import hk.quantr.executablelibrary.coff.datatype.Coff_Word;
import java.io.InputStream;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CoffRelocation {

	public Coff_Dword virtualAddress = new Coff_Dword();
	public Coff_Dword symbolTableIndex = new Coff_Dword();
	public Coff_Word type = new Coff_Word();

	void read(InputStream inputStream) throws Exception {
		virtualAddress.read(inputStream);
		symbolTableIndex.read(inputStream);
		type.read(inputStream);
	}

	public String toString() {
		String s = "";

		s += String.format("%-30s %s %n", "VirtualAddress", virtualAddress);
		s += String.format("%-30s %s %n", "SymbolTableIndex", symbolTableIndex);
		s += String.format("%-30s %s %n", "Type", type);

		return s;
	}
}
