package hk.quantr.executablelibrary.coff;

import hk.quantr.executablelibrary.coff.datatype.Coff_Dword;
import hk.quantr.executablelibrary.coff.datatype.Coff_Word;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CoffHeader {

	public Coff_Word machine = new Coff_Word();
	public Coff_Word numberOfSections = new Coff_Word();
	public Coff_Dword timeDateStamp = new Coff_Dword();
	public Coff_Dword pointerToSymbolTable = new Coff_Dword();
	public Coff_Dword numberOfSymbols = new Coff_Dword();
	public Coff_Word sizeOfOptionalHeader = new Coff_Word();
	public Coff_Word characteristics = new Coff_Word();
	public byte[] optionalHeader;

	void read(InputStream inputStream) throws Exception {
		machine.read(inputStream);
		numberOfSections.read(inputStream);
		timeDateStamp.read(inputStream);
		pointerToSymbolTable.read(inputStream);
		numberOfSymbols.read(inputStream);
		sizeOfOptionalHeader.read(inputStream);
		characteristics.read(inputStream);

		optionalHeader = new byte[(int) sizeOfOptionalHeader.getInt()];
		inputStream.read(optionalHeader);
	}

	void write(RandomAccessFile fo) throws IOException {
		fo.write(machine.bytes);
	}

	public String toString() {
		String s = "";

		s += String.format("%-30s %s %n", "Machine", machine);
		s += String.format("%-30s %s %n", "NumberOfSections", numberOfSections);
		s += String.format("%-30s %s %n", "TimeDateStamp", timeDateStamp);
		s += String.format("%-30s %s %n", "PointerToSymbolTable", pointerToSymbolTable);
		s += String.format("%-30s %s %n", "NumberOfSymbols", numberOfSymbols);
		s += String.format("%-30s %s %n", "SizeOfOptionalHeader", sizeOfOptionalHeader);
		s += String.format("%-30s %s %n", "Characteristics", characteristics);

		return s;
	}
}
