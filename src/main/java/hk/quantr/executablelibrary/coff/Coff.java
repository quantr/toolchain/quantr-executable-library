/*
 * Copyright 2019 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.executablelibrary.coff;

import hk.quantr.javalib.CommonLib;
import java.util.ArrayList;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.Arrays;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Coff implements Serializable {

	public CoffHeader coffHeader = new CoffHeader();
	public ArrayList<CoffSection> coffSections = new ArrayList<CoffSection>();
	public ArrayList<CoffSymbol> symbols = new ArrayList<CoffSymbol>();

	public static int IMAGE_SYM_CLASS_END_OF_FUNCTION = -1;
	public static int IMAGE_SYM_CLASS_NULL = 0;
	public static int IMAGE_SYM_CLASS_AUTOMATIC = 1;
	public static int IMAGE_SYM_CLASS_EXTERNAL = 2;
	public static int IMAGE_SYM_CLASS_STATIC = 3;
	public static int IMAGE_SYM_CLASS_REGISTER = 4;
	public static int IMAGE_SYM_CLASS_EXTERNAL_DEF = 5;
	public static int IMAGE_SYM_CLASS_LABEL = 6;
	public static int IMAGE_SYM_CLASS_UNDEFINED_LABEL = 7;
	public static int IMAGE_SYM_CLASS_MEMBER_OF_STRUCT = 8;
	public static int IMAGE_SYM_CLASS_ARGUMENT = 9;
	public static int IMAGE_SYM_CLASS_STRUCT_TAG = 10;
	public static int IMAGE_SYM_CLASS_MEMBER_OF_UNION = 11;
	public static int IMAGE_SYM_CLASS_UNION_TAG = 12;
	public static int IMAGE_SYM_CLASS_TYPE_DEFINITION = 13;
	public static int IMAGE_SYM_CLASS_UNDEFINED_STATIC = 14;
	public static int IMAGE_SYM_CLASS_ENUM_TAG = 15;
	public static int IMAGE_SYM_CLASS_MEMBER_OF_ENUM = 16;
	public static int IMAGE_SYM_CLASS_REGISTER_PARAM = 17;
	public static int IMAGE_SYM_CLASS_BIT_FIELD = 18;
	public static int IMAGE_SYM_CLASS_BLOCK = 100;
	public static int IMAGE_SYM_CLASS_FUNCTION = 101;
	public static int IMAGE_SYM_CLASS_END_OF_STRUCT = 102;
	public static int IMAGE_SYM_CLASS_FILE = 103;
	public static int IMAGE_SYM_CLASS_SECTION = 104;
	public static int IMAGE_SYM_CLASS_WEAK_EXTERNAL = 105;
	public static int IMAGE_SYM_CLASS_CLR_TOKEN = 107;

	public ArrayList<String> strings = new ArrayList<>();

	public Coff() {
	}

	public Coff(InputStream inputStream) throws Exception {
		read(inputStream);
	}

	void read(InputStream inputStream) throws Exception {
		byte bytes[] = inputStream.readAllBytes();
		inputStream = new ByteArrayInputStream(bytes);

		coffHeader.read(inputStream);
		for (int x = 0; x < coffHeader.numberOfSections.getInt(); x++) {
			CoffSection coffSection = new CoffSection();
			coffSection.read(inputStream);
			coffSections.add(coffSection);
		}

		for (CoffSection coffSection : coffSections) {
			// read section bytes
			inputStream = new ByteArrayInputStream(bytes);
			CommonLib.skip(inputStream, coffSection.pointerToRawData.getInt());
			coffSection.bytes = new byte[(int) coffSection.sizeOfRawData.getInt()];
			inputStream.read(coffSection.bytes);

			// read relocations per section
			inputStream = new ByteArrayInputStream(bytes);
			CommonLib.skip(inputStream, coffSection.pointerToRelocations.getInt());
			for (int x = 0; x < coffSection.numberOfRelocations.getInt(); x++) {
				CoffRelocation relocation = new CoffRelocation();
				relocation.read(inputStream);
				coffSection.relocations.add(relocation);
			}
		}

		// read string table
		inputStream = new ByteArrayInputStream(bytes);
		CommonLib.skip(inputStream, coffHeader.pointerToSymbolTable.getInt() + coffHeader.numberOfSymbols.getInt() * 18);
		long stringTableSize = CommonLib.read(inputStream, 4) - 4;
		byte tempBytes[] = new byte[(int) stringTableSize];
		inputStream.read(tempBytes);
		String stringTable = new String(tempBytes);
		strings.addAll(Arrays.asList(stringTable.split(Character.toString((char) 0))));

		// read symbol table
		inputStream = new ByteArrayInputStream(bytes);
		CommonLib.skip(inputStream, coffHeader.pointerToSymbolTable.getInt());
		for (int x = 0; x < coffHeader.numberOfSymbols.getInt(); x++) {
			CoffSymbol symbol = new CoffSymbol(stringTable, strings);
			symbol.read(inputStream);
			if (symbol.numberOfAuxSymbols.getInt() == 1) {
				if (symbol.storageClass.getInt() == IMAGE_SYM_CLASS_FILE) {
					byte temp[] = new byte[18];
					inputStream.read(temp);
					symbol.auxiliarySymbolRecords.put("filename", new String(temp));
				} else if (symbol.storageClass.getInt() == IMAGE_SYM_CLASS_STATIC) {
					symbol.auxiliarySymbolRecords.put("Length", CommonLib.read(inputStream, 4));
					symbol.auxiliarySymbolRecords.put("NumberOfRelocations", CommonLib.read(inputStream, 2));
					symbol.auxiliarySymbolRecords.put("NumberOfLinenumbers", CommonLib.read(inputStream, 2));
					symbol.auxiliarySymbolRecords.put("CheckSum", CommonLib.read(inputStream, 4));
					symbol.auxiliarySymbolRecords.put("Number", CommonLib.read(inputStream, 2));
					symbol.auxiliarySymbolRecords.put("Selection", CommonLib.read(inputStream, 1));
					symbol.auxiliarySymbolRecords.put("Unused", CommonLib.read(inputStream, 3));
				}
				x++;
			}
			symbols.add(symbol);
		}
	}

	@Override
	public String toString() {
		String s = "";
		s += coffHeader + "\n";
		for (CoffSection coffSection : coffSections) {
			s += coffSection + "\n";
		}
		s += "Symbols\n";
		for (CoffSymbol symbol : symbols) {
			s += symbol + "\n";
		}
		return s;
	}

	public void write(RandomAccessFile fo) throws IOException {

	}

}
