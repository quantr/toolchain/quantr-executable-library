Helper library to manipulate Quantr/ELF/Mach-O/PE executable format. Quantr assembler is using this library to produce object file.

# Author

Peter <peter@quantr.hk> , System Architect

Yin <yin0224ch@gmail.com> , Programmer